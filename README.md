# GeneralPermafrostToolkit

This repos is **ONLY** for evaluation of the following under-review paper:

Kang Wang. General Permafrost Toolkit (GPT) v1.0: A toolbox and Graphical User Interface (GUI) for permafrost
simulation. Environmental Modelling & Software, under review.


Online docs: http://generalpermafrosttoolkit.readthedocs.io/