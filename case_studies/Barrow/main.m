clc;clear; close all;

set(0,'DefaultFigureVisible', 'on');

mod1 = PF_Core;
uts  = PF_Utils;

initial_running     = 0
calibration_running = 0
check_calibration   = 1

%% ===== READ SOMETHINGS FROM CFG FILE =====

if initial_running == 1
    
    cfg_file = 'configuration.txt';
    
    mod1 = uts.load_cfg_file(cfg_file, mod1);
    
    mod1 = mod1.initialize();
    
    for i = 1:mod1.MN
        mod1 = mod1.update(i);
    end
    
    fig0 = uts.quick_compare_obs(mod1);
    suptitle('A) Validation before calibration');
    uts.set_figure_output_size(fig0, [7.5*1.5,15*1.5]);
    print(fig0, '-dpng', '-r300', 'pre_calibration.png');
    save('mod_inital_run.mat','mod1'); % Raw running
    
end

%============

if calibration_running == 1
    
    mod0 = load('mod_inital_run.mat');
    mod1 = mod0.mod1;
    
    x0   = [mod1.VWC' mod1.UWC_a'];
    
    maxn   = 1000;
    kstop  = 10;
    pcento = 0.1;
    iseed  = round(rand*100000000);
    iniflg = 0;
    ngs    = 2;
    
    up_perc = 0.2;
    bt_perc = 0.2;
    
    bl = x0 .* (1-bt_perc) ;
    bu = x0 .* (1+up_perc) ;
    
    [bestx,bestf] = uts.sceua('fun_soil_para', ...
        x0,...
        bl,...
        bu,...
        maxn,...
        kstop,...
        pcento,...
        ngs,...
        iseed,...
        iniflg);
    
    %====================
    
    new_par = reshape(bestx, numel(bestx)/2, 2);
    
    mod0 = load('mod_inital_run.mat');
    
    mod2 = mod0.mod1;
    
    mod2.VWC   = new_par(:,1);
    mod2.UWC_a = new_par(:,2);
    
    new_par_table = [mod2.VWC mod2.UWC_a mod2.UWC_b mod2.HC_a mod2.HC_b mod2.Ksoil mod2.Thick];
    
    for i = 1:mod2.MN
        mod2 = mod2.update(i);
    end
    
    fig1 = uts.quick_compare_obs(mod2, 1);
    uts.set_figure_output_size(fig1, [7.5*1.5,15*1.5]);
    
    print(fig1, '-dpng', '-r300', 'calibration.png');
    
    uts.quick_write_geo_parameters(mod2,'geo_calibrated.txt');
    
end

if check_calibration == 1
    
    cfg_file = 'configuration.txt';
    
    mod1 = uts.load_cfg_file(cfg_file, mod1);
    mod1.GEOPAR_IN = 'geo_calibrated.txt';
    
    mod1 = mod1.initialize();
    
    for i = 1:mod1.MN
        mod1 = mod1.update(i);
    end
    
    fig0 = uts.quick_compare_obs(mod1);
    suptitle('B) Validation after calibration');
    uts.set_figure_output_size(fig0, [7.5*1.5,15*1.5]);
    print(fig0, '-dpng', '-r300', 'post_calibration.png');
    
end