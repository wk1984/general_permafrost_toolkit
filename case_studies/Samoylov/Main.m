clc;clear;close all;

mod1 = PF_Core;
uts = PF_Utils;

initial_running     = 0;
calibration_running = 0;
calibration_snow_running = 0;
check_calibration   = 1;

if initial_running==1
    
    %% This part is to use soil surface temperature to force the model once or twice
    % in order to bring initial condition influence down.
    % write the final time step temperature profile as new initial
    % condition for calibration.
    % DONT run too many times.
    
    mod1 = uts.load_cfg_file('cfg_calibration_month.txt',mod1);
    mod1.TA_IN = 'monthly_data/Tsurface.dat';
    mod1.SIM_TYPE = 1; % TO USE HEAT TRANSFER ONLY
    mod1.SD_IN = '';
    
    mod1 = mod1.initialize();
    
    for ii = 1:2
        for  i = 1:mod1.MN
            mod1 = mod1.update(i);
        end
    end
    
    f0 = uts.quick_compare_obs(mod1,3);
    suptitle('(A) Validation before calibration');
    uts.set_figure_output_size(f0,[24 24/3*2/1.618]);
    
    save('mod_inital_run.mat','mod1'); % Raw running
    
    uts.quick_write_temp_profile(mod1, 'monthly_data/initial_2.txt');
    
    print(f0, '-dpng', '-r600', 'pre-calibration_and_validation.png');
    
end

if calibration_running == 1
    
    %% this part is to calibrate soil parameters
    % upper boundary is set to soil surface.
    
    maxn=5000;
    kstop=10;
    pcento=0.1;
    iseed=round(rand*100000000);
    iniflg=0;
    ngs=2;
    bl=[0.10 0.10 0.10 0.10 0.001 0.001 0.001 0.001  -0.40 -0.40 -0.40 -0.40];
    bu=[0.80 0.80 0.80 0.80 0.2   0.2   0.2   0.2    -0.10 -0.10 -0.10 -0.10];
    x0=[0.50 0.50 0.50 0.50 0.05  0.05  0.05  0.05   -0.35 -0.35 -0.35 -0.35];
    
    [bestx,bestf] = uts.sceua('fun_soil_para_Samoylov', x0,bl,bu,maxn,kstop,pcento,ngs,iseed,iniflg);
    
    mod0 = load('mod_inital_run.mat');
    mod1 = mod0.mod1;
    new_par = reshape(bestx, numel(bestx)/3, 3);
    
    mod1.VWC   = new_par(:,1);
    mod1.UWC_b = new_par(:,3);
    mod1.UWC_a = new_par(:,2);
    
    new_par_table = [mod1.VWC mod1.UWC_a mod1.UWC_b mod1.HC_a mod1.HC_b mod1.Ksoil mod1.Thick];
    
    for  i = 1:mod1.MN
        
        mod1 = mod1.update(i);
        
    end
    
    f0 = uts.quick_compare_obs(mod1,3);
    suptitle('(B) Validation after calibrating soil parameters');
    uts.set_figure_output_size(f0,[24 24/3*2/1.618]);
    print(f0, '-dpng', '-r600', 'calibration_soil.png');
    
    writematrix(new_par_table,'calibrated_geo.txt');
    
    % re-initialize the model in order to make sure they are not bring any
    % running memory from the last implement.
    
    mod1 = PF_Core;
    mod1 = uts.load_cfg_file('cfg_calibration_month.txt',mod1);
    mod1.GEOPAR_IN = 'calibrated_geo.txt';
    mod1.INITIAL_IN = 'monthly_data/initial_2.txt';
    mod1 = mod1.initialize();
    
    save('mod_calibrated_soil.mat','mod1'); % save running
    
end

if calibration_snow_running == 1
    
    %% This part is to calibrate snow density
    
    maxn=10000;
    kstop=15;
    pcento=0.1;
    iseed=round(rand*100000000);
    iniflg=0;
    ngs=2;
    
    bl=zeros(1,10)+50;
    bu=zeros(1,10)+650;
    x0=zeros(1,10)+250;
    
    [bestx,bestf] = uts.sceua('fun_snow_density', x0,bl,bu,maxn,kstop,pcento,ngs,iseed,iniflg);
    
    NEW_RHOSNOW = zeros(12,1);
    NEW_RHOSNOW(1:6)   = bestx(1:6);
    NEW_RHOSNOW(9:12) = bestx(7:end);
    
    writematrix(NEW_RHOSNOW,'calibrated_snow_density.txt');
    
    mod0 = load('mod_calibrated_soil.mat');
    mod1 = mod0.mod1;
    
    mod1.RHOSNOW(:)     = 200;
    
    for  i = 1:mod1.MN
        
        mod1 = mod1.update(i);
        
    end
    
    f0 = uts.quick_compare_obs(mod1,3);
    suptitle('(C) Validation before calibrating snow density');
    uts.set_figure_output_size(f0,[24 24/3*2/1.618]);
    print(f0, '-dpng', '-r600', 'calibration_before_calibration_snow.png');
    
    
end
if check_calibration==1
    
    mod1 = uts.load_cfg_file('cfg_calibration_month.txt',mod1);
    
    mod1.GEOPAR_IN = 'calibrated_geo.txt';
    mod1.SR_IN     = 'calibrated_snow_density.txt';
    mod1.INITIAL_IN = 'monthly_data/initial_2.txt';
    mod1.MN = 12;
    
    mod1 = mod1.initialize();
    
    for  i = 1:mod1.MN
        mod1 = mod1.update(i);
    end
    
    f0 = uts.quick_compare_obs(mod1,3);
    suptitle('(D) Validation using calibrated snow and soil parameters');
    f1 = uts.quick_plot_last_envelope(mod1);
    title({'(D) Validation using calibrated', 'snow and soil parameters'});
    uts.set_figure_output_size(f0,[24 24/3*2/1.618]);
    uts.set_figure_output_size(f1,[24/3*2/1.618, 16]);
    
    print(f0, '-dpng', '-r600', 'calibration_and_validation.png');
    print(f1, '-dpng', '-r600', 'profile_2005.png');
    
end