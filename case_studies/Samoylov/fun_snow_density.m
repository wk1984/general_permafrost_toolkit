function f = fun_snow_density(x)

% function name: functn
%     this function is used to implement core model with the
%     specific parameter set "x";

mod0 = load('mod_calibrated_soil.mat'); % copy model object

uts = PF_Utils;

mod1 = mod0.mod1;
mod1.START_TYPE = 1; 

mod1.RHOSNOW(1:6) = x(1:6);
mod1.RHOSNOW(9:12) = x(7:end);

for i = 1:mod1.MN
    mod1 = mod1.update(i);
end

obs = mod1.obs_data;
sim = mod1.O_TSOIL(mod1.NTB:mod1.MN, mod1.obs_depth_index);

obs = [obs(:)];
sim = [sim(:)];

f = uts.RMSE(sim, obs);

end