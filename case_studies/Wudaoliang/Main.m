clc;clear;close all;

mod1 = PF_Core;
uts = PF_Utils;

initial_running = 0;
calibration_running = 0;
check_calibration = 0;

if initial_running==1
    
    mod1 = uts.load_cfg_file('cfg_calibration.txt',mod1);
    
    mod1 = mod1.initialize();
    
    %     mod1.RAIRT = mod1.RAIRT * 0.99 + 2.85;
    
    for ii = 1:2
        for  i = 1:mod1.MN
            mod1 = mod1.update(i);
        end
    end
    
    f0 = uts.quick_compare_obs(mod1,3);
    
    uts.set_figure_output_size(f0,[24 24/1.618]);
    
    print(f0, '-dpng', '-r600', 'validation_before_calibration.png');
    
    save('mod_inital_run.mat','mod1'); % Raw running
    
    uts.quick_write_temp_profile(mod1, 'data/initial_2.txt');
    
end

if calibration_running == 1
    
    maxn=5000;
    kstop=10;
    pcento=0.1;
    iseed=round(rand*100000000);
    iniflg=0;
    ngs=2;
    bl=[0.10 0.10 0.10 0.10 0.10 0.001 0.001 0.001 0.001 0.001 -0.40 -0.40 -0.40 -0.40 -0.40];
    bu=[0.80 0.80 0.80 0.80 0.80 0.2   0.2   0.2   0.2   0.2   -0.10 -0.10 -0.10 -0.10 -0.10];
    x0=[0.50 0.50 0.50 0.50 0.50 0.05  0.05  0.05  0.05  0.05  -0.35 -0.35 -0.35 -0.35 -0.35];
    
    [bestx,bestf] = uts.sceua('fun_soil_para_WDL', x0,bl,bu,maxn,kstop,pcento,ngs,iseed,iniflg);
    
    mod0 = load('mod_inital_run.mat');
    mod1 = mod0.mod1;
    new_par = reshape(bestx, numel(bestx)/3, 3);
    
    mod1.VWC   = new_par(:,1);
    mod1.UWC_b = new_par(:,3);
    mod1.UWC_a = new_par(:,2);
    
    new_par_table = [mod1.VWC mod1.UWC_a mod1.UWC_b mod1.HC_a mod1.HC_b mod1.Ksoil mod1.Thick];
    
    for  i = 1:mod1.MN
        
        mod1 = mod1.update(i);
        
    end
    
    f0 = uts.quick_compare_obs(mod1,2);
    
    writematrix(new_par_table,'calibrated_geo.txt');
    
end

if check_calibration==1
    
    mod1 = uts.load_cfg_file('cfg_calibration.txt',mod1);
    
    mod1.GEOPAR_IN = 'calibrated_geo.txt';
    mod1.INITIAL_IN = 'data/initial_2.txt';
    mod1.ONSET_DATE_STRING = '2004-01-01';
    mod1.MN = 730;
    
    mod1 = mod1.initialize();
    
    for  i = 1:mod1.MN
        
        mod1 = mod1.update(i);
        
    end
    
    f0 = uts.quick_compare_obs(mod1,3);
    
    f1 = uts.quick_plot_last_envelope(mod1);
    
    uts.set_figure_output_size(f0,[24 24/1.618]);
    uts.set_figure_output_size(f1,[24/1.618, 24]);
    
    print(f0, '-dpng', '-r600', 'calibration_and_validation.png');
    print(f1, '-dpng', '-r600', 'profile_2005.png');
    
end

for change = 1:6
    
%     if change == 1
%         
%         color = 'k';
% 
%     end
    
    mod1 = PF_Core;
    mod1 = uts.load_cfg_file('cfg_calibration.txt',mod1);
    
    mod1.GEOPAR_IN = 'calibrated_geo.txt';
    mod1.INITIAL_IN = 'data/initial_2.txt';
    mod1.ONSET_DATE_STRING = '2014-01-01';
    mod1.MN = 365;
    
    mod1 = mod1.initialize();
    
    idx = find(mod1.RAIRT<0);
    
    mod1.RAIRT(idx) = mod1.RAIRT(idx) + abs(mod1.RAIRT(idx)) * (change-1)*0.1;
    
    for  i = 1:mod1.MN
        
        mod1 = mod1.update(i);
        
    end
        
    data = mod1.O_TSOIL(1:end,:);
    
    dp  = mod1.XYN(mod1.NSNOW+1:end);
    
    tmx = max(data,[],1);
    tmn = min(data,[],1);
    tvg = mean(data,1);
    
    if change==1
        tvg0 = tvg;
    end
    
    plot(tvg - tvg0, dp);  hold on;
    lss{change} = ['+', num2str(floor((change-1)*0.1*100),'%2.2d'), '%'];
    axis ij
    ylabel('Depth (m)');
    xlabel('Soil Temperature Change (^oC)');
        
end

legend(lss);