classdef PF_Core
    
    % PF_Core It contains all variables, parameters, and subroutines that
    % are used for permafrost modeling.
    
    properties
        
        NSNOW  = 21 % [1] NUMBER of SNOW NODES ABOVE GROUND SURFACE [default: 11]
        NODE        % [1] NUMBER of ALL NODES INCLUDING SNOW and SOIL
        NSOIL_NODE  % [1] NUMBER of SOIL NODES BELLOW GROUND SURFACE
        NSOIL_LAYER % [1] NUMBER of SOIL LAYER [eg, 0-1m is a clay soil, 2-3 m is a sandy soil.]
        
        XYN     % [NODE] defined vertical space dividation
        RTT     % [NODE] vertical temperature profile at current
        DX      % [NODE] distance between nodes.
        DTT     % [NODE]
        RKKK    % [NODE] Thermal Conductivity through all nodes.
        RCCC    % [NODE] Vol. Heat Capacity through all nodes.
        
        RTS0    % [1] Snow/ground surface temperature in the last time step
        
        VWC     % [NSOIL_LAYER] Vol. water content
        UWC_a   % [NSOIL_LAYER] unfrozen water parameter a
        UWC_b   % [NSOIL_LAYER] unfrozen water parameter b
        HC_a    % [NSOIL_LAYER] soil heat capacity parameter a
        HC_b    % [NSOIL_LAYER] soil heat capacity parameter b
        Ksoil   % [NSOIL_LAYER] soil thermal conductivity
        Thick   % [NSOIL_LAYER] soil layer thickness
        
        NTB         = 1       % [1] Offset of starting running, it's better to simulate started from summer time. [default: 1]
        NDAY        = 365     % [1] How many time steps in an annual cycle, i.e., 365 = daily, 12 = monthly. [default: 365]
        MN          = 730     % [1] Total time steps from start.
        SEC_IN_STEP = 86400.  % [1] How many seconds in a time step. [default: 86400]
        RDELTAT     = 1.0     % [1] Time delta [default: 1]
        
        ONSET_DATE_STRING = '1995-07-01'  %[1] DATETIME VARIABLE USED FOR PLOT.
        
        RKSNOW_TYPE = 'Default' % [STRING] Chose different equation to bridge snow density and thermal conducitivity.
        % Available options:
        % 'Sturm_1997','Bracht_1949','Abels_1892','Kondraleva_1954'
        
        RELOAD_CLIMATE_FILES = 1;
        
        %% Upper node:
        
        LMN = [];
        
        %%% ALL INPUTS:
        
        RAIRT           % [MN] Air (ground surface) temperature forcing, upper boundary [unit: C]
        RSNOW           % [MN] Snow Depth [unit: m]
        RHOSNOW         % [MN] Snow Density [unit: kg/m3]
        
        RRADI           % [MN] Solar Radiation  [unit: W/m2]
        RWIND           % [MN] Wind Speed       [unit: m/s ]
        RPRE            % [MN] Surface Pressure [unit: Pa  ]
        RDEWP           % [MN] Dewpoint temperature [unit: C]
        
        RTIME           % [MN] Order since NTB through MN
        
        QQ         = 0.0;    % [1] Bottom Heat Flux [default: 0]
        RHOWATER   = 1000.;  % [1] Water Density [default: 1000]
        EET        = 0.001;  % [1] Thereshold value in numerical calculation [default: 0.001]
        EES        = 0.001;  % [1] Thereshold value in numerical calculation [default: 0.001]
        RTF        = 273.15; % [1] Offset between degree C and K [default: 273.15]
        GRAVITY    = 9.8;    % [1] Gravitational acceleration [default: 9.8]
        
        ZSNOW   = 0.001;     % [1] Snow roughness length [default: 0.001]
        EMISNOW = 0.98;      % [1] Snow emissivity [default: 0.980]
        ASOIL   = 0.17;      % [1] Snow albedo [default: 0.170]
        ZSOIL   = 0.015;     % [1] Soil roughness length [default: 0.015]
        EMISOIL = 0.92;      % [1] Soil emissivity [default: 0.92]
        
        ROA = 1.275;         % [1] Air density [default: 1.275]
        CPA = 1004.0;        % [1] specific heat of air [default: 1004.0]
        
        VONK = 0.4;          % [1] Von Karman?s constant [default: 0.40]
        SIGMA = 5.67051E-8;  % [1] Stefan-Boltzmann constant [default: 5.67051E-8]
        RLS = 2.5E6;         % [1] latent heat of sublimation [default: 2.5E6]
        
        RZZH = 2.0;          % [1] Reference height of wind speed [default: 2.0]
        
        SIM_TYPE   = 0;      % [1] To set which type will be implemented, 0: EBM+HT; 1:HT [default: 0]
        START_TYPE = 1;      % [1] 0: from initial repeating to get equilibrium; 1: from now [default:1]
        
        SND_FIX    = -999;   % [1] Fix snow depth to SND_FIX, if SND_FIX >=0;
        SDEN_FIX   = -999;   % [1] Fix snow density to SDEN_FIX, if SDEN_FIX >0;
        
        % Following input files could be more than 1 column but put the
        % data in the last column.
        
        TA_IN                % [STRING] Upper boundary condition file name
        SD_IN                % [STRING] Snow depth file name
        SR_IN                % [STRING] Snow density file name
        TDEW_IN              % [STRING] Dew point temperature file name
        RAD_IN               % [STRING] Solar radiation file name
        WIND_IN              % [STRING] Wind speed file name
        PRES_IN              % [STRING] Surface pressure file name
        
        INITIAL_IN           % [STRING] Initial temperature profile file name
        GEOPAR_IN            % [STRING] Geothermal paramaters file name
        VGRID_IN             % [STRING] Node setup file name
        
        % OUTPUTS
        
        O_AQSI          % [MN] OUTPUT - AQSI
        O_QLI           % [MN] OUTPUT - QLI
        O_QLE           % [MN] OUTPUT - QLE
        O_QH            % [MN] OUTPUT - QH
        O_QE            % [MN] OUTPUT - QE
        O_QC            % [MN] OUTPUT - QC
        
        O_TSOIL         % [MN x NSOIL_NODE] OUTPUT - Ground Temperatures
        
        %=============
        % only for GUI
        obs_layer_01    % [MN] Observational ground temperature at layer 01
        obs_layer_02    % [MN] Observational ground temperature at layer 02
        obs_file_01     % [1]  Layer 01 file name
        obs_depth_01    % [1] Soil Node Index of layer 01
        obs_depth_02    % [1] Soil Node Index of layer 02
        obs_file_02     % [1]  Layer 02 file name
        %=============
        
        n_obs_depth     % [1] number of soil nodes to be validated
        obs_files = {}  % [N] Cells for storing strings of file names
        obs_depth       % [N] Soil depths obervations.
        obs_depth_index % [N] Soil Node Index of layers
        obs_data        % [MN, N] Observational ground temperature
        
    end
    
    methods
        
        function obj = initialize(obj)
            
            % PF_Core/initial Set some variables to a initial point
            % EXAMPLE:
            %        obj = obj.initial();
            
            obj.NDAY  = 365*86400 / obj.SEC_IN_STEP;
            
            obj = obj.load_climate_file();
            
            obj = obj.load_geothermal_para();
            
            obj = obj.load_grid_setup_file();
            
            obj = obj.load_initial_temperature();
            
            obj = obj.XNODES();
            
            %             obj.O_TSOIL = zeros(1, obj.NSOIL_NODE);
            
            %             obj.O_AQSI = zeros(obj.MN, 1);
            
        end
        
        function obj = update(obj, MNTIME)
            
            % PF_Core/update Run model one time step.
            % EXAMPLE:
            %        obj = obj.update(MNTIME)
            
            if mod(MNTIME, obj.NDAY) == 0
                IYEAR = MNTIME / obj.NDAY;
            else
                IYEAR = floor(MNTIME / obj.NDAY) + 1;
            end
            IDAY = MNTIME - (IYEAR-1)*obj.NDAY;
            
            if MNTIME == obj.NTB
                TMP_SUR0 = obj.RTT(obj.NSNOW+1);
                obj.RKKK(obj.NSNOW + 2)  = 0.56 * obj.SEC_IN_STEP;
                obj.RTS0 = obj.RTF + obj.RAIRT(obj.NTB);
            end
            
            %% CORE START
            %  RUN MODUELS AT EACH TIME STEP:
            
            RSNOWH = obj.RSNOW(MNTIME);
            ROSNOW = obj.RHOSNOW(MNTIME);
            TA     = obj.RAIRT(MNTIME) + obj.RTF;
            
            if obj.SIM_TYPE == 0
                TMIN   = obj.RDEWP(MNTIME) + obj.RTF;
                QSI    = obj.RRADI(MNTIME);
                PA     = obj.RPRE(MNTIME);
                UWINDZ = obj.RWIND(MNTIME);
            end
            
            [RKSNOW, RCSNOW, ASNOW] = obj.SUBRKSNOW(ROSNOW);
            
            obj.RKKK(1:obj.NSNOW) = RKSNOW;
            obj.RCCC(1:obj.NSNOW) = RCSNOW;
            
            [obj,LMN] = obj.SUBSETSNOWLAYER(RSNOWH, TA - obj.RTF);
            
            if ~isempty(obj.LMN)
                LMN = obj.LMN;
            end
            
            if obj.SIM_TYPE == 0
                
                XRTT7  = obj.RTT(obj.NSNOW + 2) + obj.RTF;
                XXYN7  = obj.XYN(obj.NSNOW + 2);
                RKKK7  = obj.RKKK(obj.NSNOW + 2);
                
                if RSNOWH > 0.001
                    AQSI = (1-ASNOW)*QSI;
                else
                    AQSI = (1-obj.ASOIL)*QSI;
                end
                
                EA  = obj.SUBEA(TMIN);
                QLI = obj.SUBQLI(TA, EA);
                
                NCONTRALER = 1;
                
                while(1)
                    
                    QLE      = obj.SUBQLE(obj.RTS0, RSNOWH);
                    DXQLE    = obj.SUBDXQLE(obj.RTS0, RSNOWH);
                    [DH, DE] = obj.SUBDHDE(UWINDZ, RSNOWH);
                    [~, XI] = obj.SUBRIXI(TA, obj.RTS0, UWINDZ);
                    ES0      = obj.SUBES0(obj.RTS0);
                    QH       = obj.SUBQH(DH, XI, TA, obj.RTS0);
                    DXQH     = obj.SUBDXQH(UWINDZ, TA, obj.RTS0, RSNOWH);
                    QE       = obj.SUBQE(DE,XI,EA,ES0,PA);
                    DXQE     = obj.SUBDXQE(UWINDZ,EA, ES0, PA, TA, obj.RTS0, RSNOWH);
                    QC       = obj.SUBQC(RSNOWH, XRTT7, XXYN7, RKKK7, RKSNOW, obj.RTS0);
                    DXQC     = obj.SUBDXQC(RSNOWH, XXYN7, RKKK7, RKSNOW);
                    
                    RFTS0    = AQSI + QLI + QLE + QH + QE + QC;
                    DXRFTS0  = DXQLE + DXQH + DXQE + DXQC;
                    TEMRTS0  = obj.RTS0 - RFTS0 / DXRFTS0;
                    
                    if abs(TEMRTS0 - obj.RTS0) < obj.EES
                        
                        if RSNOWH > 0.001 && obj.RTS0 > obj.RTF
                            obj.RTS0 = obj.RTF;
                            QH = obj.SUBQH(DH, XI, TA, obj.RTS0);
                            QLE = obj.SUBQLE(obj.RTS0, RSNOWH);
                            QE  = obj.SUBQE(DE,XI,EA,ES0,PA);
                            QC  = obj.SUBQC(RSNOWH, XRTT7, XXYN7, RKKK7, RKSNOW, obj.RTS0);
                        else
                            obj.RTS0 = TEMRTS0;
                        end
                        break
                        
                    else
                        
                        NCONTRALER = NCONTRALER + 1;
                        if NCONTRALER >= 1001
                            break
                        else
                            obj.RTS0 = TEMRTS0;
                        end
                    end
                end
                
                obj.RTT(LMN)  = obj.RTS0 - obj.RTF;
                
                %% SAVE EBM OUTPUTS:
                obj.O_AQSI(MNTIME,1) = AQSI;
                obj.O_QLI(MNTIME ,1) = QLI;
                obj.O_QLE(MNTIME ,1) = QLE;
                obj.O_QH(MNTIME  ,1) = QH;
                obj.O_QE(MNTIME  ,1) = QE;
                obj.O_QC(MNTIME  ,1) = QC;
                
            else
                
                obj.RTT(LMN) = TA - obj.RTF;
                
            end
            
            if IYEAR==1 && IDAY == obj.NTB
                
                if obj.START_TYPE == 0
                    
                    RSNOWH = 0;
                    LMN = obj.NSNOW + 1;
                    obj.RTT(LMN) = TMP_SUR0;
                    
                end
                
            end
            
            %% HEAT TRANSFER:
            while(1)
                [THETAI, THETAU, RLL, DXTHETAU] = obj.SUBTHETA();
                
                obj = obj.SUBRCCC(THETAI, THETAU, RLL, DXTHETAU);
                obj = obj.SUBRKKK(THETAI, THETAU);
                [A,B,C,D] = obj.ABCD(LMN);
                obj = obj.TRAM(LMN, A, B, C, D);
                
                if IYEAR ~= 1 || IDAY ~= obj.NTB
                    obj.RTT((LMN+1):obj.NODE) = obj.DTT((LMN+1):obj.NODE);
                    break
                else
                    %
                    if obj.START_TYPE == 0
                        %
                        REEE = abs(obj.RTT((LMN+1):obj.NODE) - obj.DTT((LMN+1):obj.NODE));
                        %
                        if REEE<=obj.EET
                            obj.RTT((LMN+1):obj.NODE) = obj.DTT((LMN+1):obj.NODE);
                            break
                        else
                            obj.RTT((LMN+1):obj.NODE) = obj.DTT((LMN+1):obj.NODE);
                        end
                        
                    else
                        
                        obj.RTT((LMN+1):obj.NODE) = obj.DTT((LMN+1):obj.NODE);
                        break
                        
                    end
                end
            end
            
            obj.O_TSOIL(MNTIME, :) = obj.RTT(obj.NSNOW+1:obj.NODE);
            
        end
        
        function obj = run(obj)
            
            % PF_Core/run Run model for all time steps.
            % EXAMPLE:
            %        obj = obj.run()
            
            obj = obj.initialize();
            
            for ii = obj.NTB:obj.MN
                
                MNTIME = ii;
                
                obj = obj.update(MNTIME);
                
            end
            
        end
        
        %% Heat Transfer Functions:
        
        function obj = TRAM(obj, LMN, A, B, C, D)
            
            % PF_Core/TRAM To solve tridiagnal equations algorithm
            % INPUTs:
            %        LMN: [1] Top node
            %        A  : [NODE]
            %        B  : [NODE]
            %        C  : [NODE]
            %        D  : [NODE]
            % EXAMPLE:
            %        obj = obj.TRAM(LMN, A, B, C, D)
            
            RP = zeros(obj.NODE,1);
            RQ = zeros(obj.NODE,1);
            
            RP(LMN + 1) = -C(LMN + 1)/B(LMN + 1);
            
            RQ(LMN + 1) = D(LMN + 1)/B(LMN + 1);
            for I = LMN + 2: obj.NODE
                PP = A(I) * RP(I - 1) + B(I);
                RP(I) = -C(I)/PP;
                RQQ = D(I) - A(I) * RQ(I - 1);
                RQ(I) = RQQ/PP;
            end
            
            
            % ---------------------BACK SUBSTITUTION-------------------
            obj.DTT(obj.NODE) = RQ(obj.NODE);
            for I = obj.NODE - 1:-1:LMN + 1
                obj.DTT(I) = RP(I) * obj.DTT(I + 1) + RQ(I);
            end
            
        end
        
        function [A,B,C,D] = ABCD(obj, LMN)
            
            % PF_Core/ABCD Set ABCD for tridiagnal equations.
            % Input:
            %        LMN: [1] Top node
            % Outputs:
            %        A  : [NODE]
            %        B  : [NODE]
            %        C  : [NODE]
            %        D  : [NODE]
            % EXAMPLE:
            %        [A,B,C,D] = obj.ABCD(LMN)
            
            NGRND = obj.NSNOW + 1;
            
            A = zeros(obj.NODE,1);
            B = zeros(obj.NODE,1);
            C = zeros(obj.NODE,1);
            D = zeros(obj.NODE,1);
            
            RKW = zeros(obj.NODE,1);
            RKE = zeros(obj.NODE,1);
            
            RCW = zeros(obj.NODE,1);
            RCE = zeros(obj.NODE,1);
            
            RCCCP = zeros(obj.NODE,1);
            RDX = zeros(obj.NODE,1);
            APZERO = zeros(obj.NODE,1);
            
            RKW(LMN + 1:NGRND) = obj.RKKK(LMN:NGRND - 1);
            RKW(NGRND + 1:obj.NODE - 1) = 2 * obj.RKKK(NGRND:obj.NODE - 2) .* ...
                obj.RKKK(NGRND + 1:obj.NODE - 1)./...
                (obj.RKKK(NGRND:obj.NODE - 2) + obj.RKKK(NGRND + 1:obj.NODE - 1));
            
            RKE(LMN + 1:NGRND - 1) = obj.RKKK(LMN + 1:NGRND - 1);
            RKE(NGRND:obj.NODE - 1) = 2 * obj.RKKK(NGRND:obj.NODE - 1) .* ...
                obj.RKKK(NGRND + 1:obj.NODE)./...
                (obj.RKKK(NGRND:obj.NODE - 1) + obj.RKKK(NGRND + 1:obj.NODE));
            
            A(LMN + 1:obj.NODE - 1) = RKW(LMN + 1:obj.NODE - 1)./obj.DX(LMN:obj.NODE - 2);
            C(LMN + 1:obj.NODE - 1) = RKE(LMN + 1:obj.NODE - 1)./obj.DX(LMN + 1:obj.NODE - 1);
            
            RCW(LMN + 1:obj.NODE - 1) = obj.RCCC(LMN:obj.NODE - 2) .* obj.DX(LMN:obj.NODE - 2);
            RCE(LMN + 1:obj.NODE - 1) = obj.RCCC(LMN + 2:obj.NODE) .* obj.DX(LMN + 1:obj.NODE - 1);
            
            RCCCP(LMN + 1:obj.NODE - 1) = (RCW(LMN + 1:obj.NODE - 1) + ...
                RCE(LMN + 1:obj.NODE - 1))./...
                (obj.DX(LMN:obj.NODE - 2) + obj.DX(LMN + 1:obj.NODE - 1));
            
            RDX(LMN + 1:obj.NODE - 1) = (obj.DX(LMN:obj.NODE - 2) + ...
                obj.DX(LMN + 1:obj.NODE - 1))/2.0;
            
            APZERO(LMN + 1:obj.NODE - 1) = RCCCP(LMN + 1:obj.NODE - 1) .* ...
                RDX(LMN + 1:obj.NODE - 1)/obj.RDELTAT;
            
            B(LMN + 1:obj.NODE - 1) = -(A(LMN + 1:obj.NODE - 1) + ...
                C(LMN + 1:obj.NODE - 1) + APZERO(LMN + 1:obj.NODE - 1));
            
            D(LMN + 1) = -APZERO(LMN + 1) * obj.RTT(LMN + 1) - A(LMN + 1) * obj.RTT(LMN);
            D(LMN + 2:obj.NODE - 1) = -APZERO(LMN + 2:obj.NODE - 1) .* obj.RTT(LMN + 2:obj.NODE - 1);
            
            A(obj.NODE) = obj.RKKK(obj.NODE - 1)./obj.DX(obj.NODE - 1);
            C(obj.NODE) = 0.0;
            B(obj.NODE) = -A(obj.NODE);
            D(obj.NODE) = -obj.QQ * obj.SEC_IN_STEP;
            
        end
        
        function obj = SUBRKKK(obj, THETAI, THETAU)
            
            % PF_Core/SUBRKKK Calculate thermal conductivity
            % CALCULATE THE THERMAL CONDUCTIVITIES OF SOIL, ICE, UNFROZEN, AND THE
            % THERMAL CONDUCTIVITY OF PERMAFROST CONTAINING UNFROZEN WATER USING
            % THE WEIGHTED GEOMETRIC MEAN EQUATION (LACHENBRUCH ET AL, 1982;
            % OSTERKAMP, 1987)
            % Input:
            %        THETAI: [NODE] Ice fraction
            %        THETAU: [NODE] Liquid water fraction
            % EXAMPLE:
            %        obj = obj.SUBRKKK(THETAI, THETAU)
            
            RKK    = obj.Ksoil;
            HIGH   = obj.Thick;
            
            N_LEVELS = numel(RKK);
            
            HIGH1   = cumsum(HIGH);
            HIGH0 = HIGH * 0.0;
            HIGH0(2:N_LEVELS) = HIGH1(1:end-1);
            
            for II = 1: N_LEVELS
                
                idx = find(obj.XYN >=HIGH0(II) & obj.XYN<=HIGH1(II));
                idx = idx(idx>obj.NSNOW);
                
                RKKICE   = 0.4685 + 488.19./(obj.RTF + obj.RTT(idx));
                RKKWATER = 0.11455 + 1.6318E-3 * (obj.RTF + obj.RTT(idx));
                RKKSOIL  = RKK(II);
                obj.RKKK(idx) = RKKICE .^ THETAI(idx) .* RKKSOIL .^(1.0 - THETAU(idx)-...
                    THETAI(idx)) .* RKKWATER .^ THETAU(idx);
                obj.RKKK(idx) = obj.RKKK(idx) * obj.SEC_IN_STEP;
                
            end
        end
        
        function obj = SUBRCCC(obj, THETAI, THETAU, RLL, DXTHETAU)
            
            % PF_Core/SUBRCCC Calculate heat capactity
            % DETERMINE THE VOLUMATRIC HEAT CAPACITIES OF SOIL, ICE, UNFROZEN, AND
            % THE APPARENT VOLUMETRIC HEAT CAPACITY FOR AQ SOIL CONTAINING UNFROZEN
            % WATER ACCORDING TO OSTERKAMP (1987).
            % Input:
            %        THETAI: [NODE] Ice fraction
            %        THETAU: [NODE] Liquid water fraction
            %        RLL   : [NODE] VOLUMETRIC LATENT HEAT FOR ICE FREEZING
            %        DXTHETAU: [NODE]
            %
            % EXAMPLE:
            %        obj = obj.SUBRCCC(THETAI, THETAU, RLL, DXTHETAU)
            
            AC     = obj.HC_a;
            BC     = obj.HC_b;
            HIGH   = obj.Thick;
            
            N_LEVELS = numel(AC);
            
            HIGH1   = cumsum(HIGH);
            HIGH0 = HIGH * 0.0;
            HIGH0(2:N_LEVELS) = HIGH1(1:end-1);
            for II = 1: N_LEVELS
                
                AAA = AC(II);
                BBB = BC(II);
                
                idx = find(obj.XYN >=HIGH0(II) & obj.XYN<=HIGH1(II));
                idx = idx(idx>obj.NSNOW);
                
                RCCICE = 1.94 + 7.14E-3 .* obj.RTT(idx);
                RCCWATER = 4.20843 + 1.11362/1E1 .* obj.RTT(idx)+...
                    5.12142/1E3 .* (obj.RTT(idx)).^2 + 9.3482/1E5 .* (obj.RTT(idx)).^3;
                
                RCCSOIL = AAA + BBB .* (obj.RTF + obj.RTT(idx));
                RCV = THETAU(idx) .* RCCWATER + THETAI(idx) .* RCCICE + ...
                    (1.0 - THETAI(idx) - THETAU(idx)) .* RCCSOIL;
                
                obj.RCCC(idx) = (RCV + RLL(idx) .* DXTHETAU(idx)) * 1.0E6;
            end
            
            
        end
        
        function [THETAI, THETAU, RLL, DXTHETAU] = SUBTHETA(obj)
            
            % PF_Core/SUBTHETA It is to determine how much ice and water in each node
            %     DETERMINE THETA_I AND THETA_U FOR EACH NODE.
            % INPUTS:
            % OUTPUTS:
            %     THETAI: [NODE] Ice content
            %     THETAU: [NODE] Liquid water content
            %     RLL   : [NODE] VOLUMETRIC LATENT HEAT FOR ICE FREEZING
            %     DXTHETAU: [NODE]
            
            THETAU = zeros(obj.NODE,1);
            THETAI = zeros(obj.NODE,1);
            RLL    = zeros(obj.NODE,1);
            DXTHETAU = zeros(obj.NODE,1);
            
            THETA0 = obj.VWC;
            UW_A   = obj.UWC_a;
            UW_B   = obj.UWC_b;
            HIGH   = obj.Thick;
            
            N_LEVELS = numel(THETA0);
            
            HIGH1   = cumsum(HIGH);
            HIGH0 = HIGH * 0.0;
            HIGH0(2:N_LEVELS) = HIGH1(1:end-1);
            
            for II = 1: N_LEVELS
                
                RAAA = UW_A(II);
                RBBB = UW_B(II);
                THETA = 1.0 - THETA0(II);
                
                RTZERO = -(THETA0(II)./RAAA).^(1./RBBB);
                
                % FROZEN:
                
                idx_frz = find(obj.XYN >=HIGH0(II) & obj.XYN<=HIGH1(II) & obj.RTT < RTZERO);
                idx_frz = idx_frz(idx_frz>obj.NSNOW);
                
                THETAU(idx_frz) = RAAA .* (abs(obj.RTT(idx_frz))) .^ RBBB ;
                if THETAU(idx_frz)>0.95
                    THETAU(idx_frz)=0.95;
                end
                THETAU(idx_frz) = THETAU(idx_frz) .* THETA0(II);
                
                THETAI(idx_frz) = 1.0 - THETA - THETAU(idx_frz);
                RLL(idx_frz) = obj.RHOWATER .* (333.2 + 4.955 .* obj.RTT(idx_frz) ...
                    + 0.02987 .* obj.RTT(idx_frz).^2)./1000.0;
                
                DXTHETAU(idx_frz) = -RAAA .* RBBB .* (abs(obj.RTT(idx_frz))).^(RBBB - 1.0).* THETA0(II);
                
                % THAW:
                
                idx_thw = find(obj.XYN >=HIGH0(II) & obj.XYN<=HIGH1(II) & obj.RTT >= RTZERO);
                idx_thw = idx_thw(idx_thw>obj.NSNOW);
                
                THETAU(idx_thw) = 1.0 - THETA;
                THETAI(idx_thw) = 0.0;
                RLL(idx_thw) = 0.0;
                DXTHETAU(idx_thw) = 0.0;
                
                
            end
            
        end
        
        %% Energy Balance Functions:
        
        function DXQC = SUBDXQC(obj, RSNOWH, XXYN7, RKKPEAT, RKSNOW)
            
            % PF_Core/SUBDXQC CALCULATE THE HEAT CONDUCTED THROUGH THE SOIL, QC.
            % INPUTS:
            %    RSNOWH : [1] Snow Depth (m)
            %    XXYN7  : [1] First node below the ground surface
            %    RKKPEAT: [1] Thermal conductivity of the first node below
            %                 the ground surface.
            %    RKSNOW : [1] Snow thermal conductivity
            % OUTPUTS:
            %    DXQC   : [1] differential of QC
            
            if RSNOWH <= 0.001
                RERKK = XXYN7 / (RKKPEAT / obj.SEC_IN_STEP);
            else
                RERKK = RSNOWH / (RKSNOW/obj.SEC_IN_STEP) + XXYN7/(RKKPEAT/obj.SEC_IN_STEP);
            end
            
            DXQC = -1.0 / RERKK;
        end
        
        function QC  = SUBQC(obj, RSNOWH, XRTT7, XXYN7, RKKPEAT, RKSNOW, RTS0)
            
            % PF_Core/SUBQC CALCULATE THE HEAT CONDUCTED THROUGH THE SOIL, QC.
            % INPUTS:
            %    RSNOWH : [1] Snow Depth (m)
            %    XXYN7  : [1] First node below the ground surface
            %    RKKPEAT: [1] Thermal conductivity of the first node below
            %                 the ground surface.
            %    RKSNOW : [1] Snow thermal conductivity
            %    RTS0   : [1] Surface temperature in the last time step
            % OUTPUTS:
            %    DXQC   : [1] HEAT CONDUCTED THROUGH THE SOIL
            
            if RSNOWH <= 0.001
                RERKK = XXYN7 / (RKKPEAT / obj.SEC_IN_STEP);
            else
                RERKK = RSNOWH / (RKSNOW / obj.SEC_IN_STEP) + XXYN7 / (RKKPEAT / obj.SEC_IN_STEP);
            end
            
            QC = -(RTS0 - XRTT7) / RERKK;
            
        end
        
        function DXQE = SUBDXQE(obj, UWINDZ,EA, ES0, PA, TA, RTS0, RSNOWH)
            
            % PF_Core/SUBDXQE CALCULATE THE TURBULENT EXCHANGE OF LATENT HEAT, QE.
            % INPUTS:
            %       UWINDZ : [1] Wind Speed
            %       EA     : [1] ATMOSPHERIC VAPOR
            %       ES0    : [1] WATER VAPOR PRESSURE AT THE SURFACE
            %       PA     : [1] Surface Pressure
            %       RTS0   : [1] Surface temperature in the last time step
            %       RSNOWH : [1] Snow Depth (m)
            % OUTPUTS:
            %       DXQE   : [1] Differential of QE
            
            if RTS0 <= TA
                if RSNOWH > 0.001
                    YDXDH1 = obj.ROA * obj.RLS * obj.VONK^2 * UWINDZ / (log(obj.RZZH / obj.ZSNOW))^2;
                else
                    YDXDH1 = obj.ROA * obj.RLS * obj.VONK^2 * UWINDZ / (log(obj.RZZH / obj.ZSOIL))^2;
                end
                
                YDXQH2 = obj.GRAVITY * obj.RZZH * (TA - RTS0) / (TA * UWINDZ^2);
                YDXQH3 = 0.622 * (EA - ES0) / PA;
                
                DXQE0 = YDXDH1 * YDXQH3 * 10.0 * obj.GRAVITY * obj.RZZH / (TA * UWINDZ^2);
                DXQE1 = DXQE0/(1.0 + 10.0 * YDXQH2)^2;
                DXQE2 = YDXDH1 * 0.622 * ES0 / PA;
                DXQE3 = 2353.0 * log(10.0) / RTS0^2 / (1.0 + 10.0 * YDXQH2);
                
                DXQE = DXQE1 - DXQE2 * DXQE3;
                
            else
                
                if RSNOWH > 0.001
                    YDXDH1 = obj.ROA * obj.RLS * obj.VONK^2 * UWINDZ / (log(obj.RZZH / obj.ZSNOW))^2;
                else
                    YDXDH1 = obj.ROA * obj.RLS * obj.VONK^2 * UWINDZ / (log(obj.RZZH / obj.ZSOIL))^2;
                end
                
                DXQE2 = 0.622 * ES0 / PA;
                DXQE3 = 2353.0 * log(10.0)/ RTS0^2;
                
                DXQE = -YDXDH1 * DXQE2 * DXQE3;
            end
            
        end
        
        function QE  = SUBQE(obj,DE,XI,EA,ES0,PA)
            
            % PF_Core/SUBQE CALCULATE THE TURBULENT EXCHANGE OF LATENT HEAT, QE.
            % INPUTS:
            %       UWINDZ : [1] Wind Speed
            %       DE     : [1]
            %       XI     : [1]
            %       EA     : [1] ATMOSPHERIC VAPOR
            %       ES0    : [1] WATER VAPOR PRESSURE AT THE SURFACE
            %       PA     : [1] Surface Pressure
            % OUTPUTS:
            %       QE     : [1] TURBULENT EXCHANGE OF LATENT HEAT
            
            QE = obj.ROA * obj.RLS * DE * XI * (0.622 * (EA - ES0) / PA);
            
        end
        
        function QH = SUBQH(obj, DH, XI, TA, RTS0)
            
            % PF_Core/SUBQH CALCULATE THE TURBULENT EXCHANGE OF SENSIBLE HEAT, QH.
            % INPUTS:
            %       DH     : [1]
            %       XI     : [1]
            %       TA     : [1] Upper Boundary
            %       RTS0   : [1] Surface temperature in the last time step
            % OUTPUTS:
            %       QH     : [1] TURBULENT EXCHANGE OF SENSIBLE HEAT
            
            QH = obj.ROA * obj.CPA * DH * XI * (TA - RTS0);
            
        end
        
        function DXQH = SUBDXQH(obj, UWINDZ, TA, RTS0, RSNOWH)
            
            % PF_Core/SUBDXQH CALCULATE THE TURBULENT EXCHANGE OF SENSIBLE HEAT, QH.
            % INPUTS:
            %       UWINDZ : [1] Wind Speed
            %       TA     : [1] Upper Boundary
            %       RTS0   : [1] Surface temperature in the last time step
            %       RSNOWH : [1] Snow Depth (m)
            % OUTPUTS:
            %       DXQH   : [1] TURBULENT EXCHANGE OF SENSIBLE HEAT
            
            if RTS0 <= TA
                if RSNOWH > 0.001
                    YDXQH1 = obj.ROA * obj.CPA * obj.VONK^2 * UWINDZ / ...
                        (log(obj.RZZH / obj.ZSNOW))^2;
                else
                    YDXQH1 = obj.ROA * obj.CPA * obj.VONK^2 * UWINDZ / ...
                        (log(obj.RZZH / obj.ZSOIL))^2;
                end
                
                YDXQH2 = obj.GRAVITY * obj.RZZH * (TA - RTS0) / (TA * UWINDZ^2);
                
                DXQH1 = YDXQH1 * 10.0 * YDXQH2 / (1.0 + 10.0 * YDXQH2)^2;
                DXQH2 = YDXQH1 / (1.0 + 10.0 * YDXQH2);
                DXQH  = DXQH1 - DXQH2;
                
            else
                if RSNOWH > 0.001
                    DXQH1 = obj.ROA * obj.CPA * obj.VONK^2 * UWINDZ / ...
                        (log(obj.RZZH / obj.ZSNOW))^2;
                else
                    DXQH1 = obj.ROA * obj.CPA * obj.VONK^2 * UWINDZ / ...
                        (log(obj.RZZH / obj.ZSOIL))^2;
                end
                
                DXQH = -DXQH1;
                
            end
            
        end
        
        function ES0 = SUBES0(obj, RTS0)
            
            ES0 = 10.0 ^ (11.40 - 2353.0 / RTS0);
            
        end
        
        function [RI, XI] = SUBRIXI(obj, TA, RTS0, UWINDZ)
            
            RI = obj.GRAVITY * obj.RZZH * (TA - RTS0) / (TA * UWINDZ^2);
            
            if RTS0 > TA
                XI = 1.0;
            else
                XI = 1.0 / (1.0 + 10.0 * RI);
            end
            
        end
        
        function [DH, DE] = SUBDHDE(obj,UWINDZ,RSNOWH)
            
            if RSNOWH > 0.001
                DH = obj.VONK^2 * UWINDZ / (log(obj.RZZH / obj.ZSNOW))^2;
            else
                DH = obj.VONK^2 * UWINDZ / (log(obj.RZZH / obj.ZSOIL))^2;
            end
            
            DE = DH;
            
        end
        
        function DXQLE = SUBDXQLE(obj, RTS0, RSNOWH)
            if RSNOWH > 0.001
                DXQLE = -4.0 * obj.EMISNOW * obj.SIGMA * RTS0^3;
            else
                DXQLE = -4.0 * obj.EMISOIL * obj.SIGMA * RTS0^3;
            end
            
        end
        
        function QLE = SUBQLE(obj,RTS0, RSNOWH)
            
            % PF_Core/SUBQLE CALCULATE THE EMITTED LONGWAVE RADIATION, QLE.
            % INPUTS:
            %       RTS0:   [1] Surface temperature in the last time step
            %       RSNOWH: [1] Snow Depth (m)
            %       QLE   : [1] EMITTED LONGWAVE RADIATION QLE
            
            if RSNOWH > 0.001
                QLE = -obj.EMISNOW * obj.SIGMA * RTS0^4;
            else
                QLE = -obj.EMISOIL * obj.SIGMA * RTS0^4;
            end
            
        end
        
        function QLI = SUBQLI(obj, TA, EA)
            
            % PF_Core/SUBQLI CALCULATE THE INCOMEING LONGWAVE RADIATION, QLI.
            % INPUTS:
            %       TA: [1] Upper boundary condition
            %       EA: [1] ATMOSPHERIC VAPOR
            % OUTPUTS:
            %       QLI: [1] INCOMEING LONGWAVE RADIATION
            
            QLI = 1.08 * (1.0 - exp(-(0.01 * EA)^(TA/2016.0))) * obj.SIGMA * TA^4;
            
        end
        
        function EA = SUBEA(obj,TMIN)
            
            % PF_Core/SUBEA CALCULATE THE ATMOSPHERIC VAPOR, EA.USE KNOWLEDGE OF THE RELATIVE HUMIDITY AND THE AIR TEMPERATURE.
            %
            % INPUTS:
            %       TMIN: [1] Dewpoint temperature
            % OUTPUTS:
            %       EA  : [1] ATMOSPHERIC VAPOR
            
            EA = 10.0 ^ (11.40 - 2353.0 / TMIN);
            
        end
        
        %% Pre-Process Functions:
        
        function obj = XNODES(obj)
            
            % PF_Core/XNODES Calculate inteval between nodes.
            obj.DX = zeros(obj.NODE,1);
            obj.RCCC = zeros(obj.NODE,1);
            obj.RKKK = zeros(obj.NODE,1);
            obj.DTT = zeros(obj.NODE, 1);
            obj.DX(obj.NSNOW+1:obj.NODE-1,1) = ...
                obj.XYN(obj.NSNOW+2:obj.NODE) - obj.XYN(obj.NSNOW+1:obj.NODE-1);
        end
        
        function [RKSNOW, RCSNOW, ASNOW] = SUBRKSNOW(obj,ROSNOW)
            
            % PF_Core/SUBRKSNOW Calculate snow thermal conductivity, heat
            %                   capacity, and albedo.
            % INPUTS:
            %       ROSNOW: [1] Snow Density
            % OUTPUTS:
            %       RKSNOW: [1] Snow thermal conductivity
            %       RCSNOW: [1] Heat Capacity
            %       ASNOW : [1] Snow surface albedo
            
            switch obj.RKSNOW_TYPE
                case 'Sturm_1997'
                    RKSNOW = 10^(2.650 * ROSNOW * 0.001 - 1.652);
                case 'Bracht_1949'
                    RKSNOW = ROSNOW* 0.001  * ROSNOW* 0.001  * 2.051;
                case 'Abels_1892'
                    RKSNOW = ROSNOW* 0.001  * ROSNOW* 0.001  * 2.846;
                case 'Kondraleva_1954'
                    RKSNOW = ROSNOW* 0.001  * ROSNOW* 0.001  * 3.558;
                otherwise
                    RKSNOW = ROSNOW * ROSNOW * 2.9E-6;
            end
            
            RCSNOW = ROSNOW * 2.09E3;
            RKSNOW = RKSNOW * obj.SEC_IN_STEP;
            
            if ROSNOW>450
                ASNOW = -ROSNOW/4600 + 0.6;
            else
                ASNOW = 1.0 - 0.247 * sqrt(0.16 + 110.0 * (ROSNOW /1000.0)^4);
            end
            
        end
        
        function [obj,LMN] = SUBSETSNOWLAYER(obj,RSNOWH, TA)
            
            % PF_Core/SUBSETSNOWLAYER Set snow layers depending on snow
            %                         thickness
            % INPUTS:
            %       RSNOWH: [1] Snow Depth (m)
            %       TA    : [1] Upper boundary
            % OUTPUTS:
            %       LMN   : [1] Top node index
            
            if RSNOWH <= 0.001
                LMN = obj.NSNOW + 1;
            elseif RSNOWH <= 0.01
                LMN = obj.NSNOW ;
            elseif RSNOWH <= 0.02
                LMN = obj.NSNOW - 1;
            elseif RSNOWH <= 0.03
                LMN = obj.NSNOW - 2;
            elseif RSNOWH <= 0.04
                LMN = obj.NSNOW - 3;
            elseif RSNOWH <= 0.05
                LMN = obj.NSNOW - 4;
            elseif RSNOWH <= 0.07
                LMN = obj.NSNOW - 5;
            elseif RSNOWH <= 0.09
                LMN = obj.NSNOW - 6;
            elseif RSNOWH <= 0.12
                LMN = obj.NSNOW - 7;
            elseif RSNOWH <= 0.15
                LMN = obj.NSNOW - 8;
            elseif RSNOWH <= 0.18
                LMN = obj.NSNOW - 9;
            elseif RSNOWH <= 0.21
                LMN = obj.NSNOW - 10;
            elseif RSNOWH <= 0.24
                LMN = obj.NSNOW - 11;
            elseif RSNOWH <= 0.27
                LMN = obj.NSNOW - 12;
            elseif RSNOWH <= 0.30
                LMN = obj.NSNOW - 13;
            elseif RSNOWH <= 0.35
                LMN = obj.NSNOW - 14;
            elseif RSNOWH <= 0.40
                LMN = obj.NSNOW - 15;
            elseif RSNOWH <= 0.45
                LMN = obj.NSNOW - 16;
            elseif RSNOWH <= 0.50
                LMN = obj.NSNOW - 17;
            elseif RSNOWH <= 0.60
                LMN = obj.NSNOW - 18;
            elseif RSNOWH <= 0.70
                LMN = obj.NSNOW - 19;
            else
                LMN = obj.NSNOW - 20;
            end
            
            if LMN <= obj.NSNOW
                for i = LMN:obj.NSNOW
                    obj.XYN(i) = -RSNOWH / (obj.NSNOW + 1- LMN)* (obj.NSNOW + 1- i);
                end
                
                obj.RTT(LMN:obj.NSNOW) = TA;
                obj.DX(LMN:obj.NSNOW)  = RSNOWH / (obj.NSNOW +  1 - LMN);
            end
            
        end
        
        function obj = load_climate_file(obj)
            
            reload = obj.RELOAD_CLIMATE_FILES;
            
            % read climate data
            if isempty(obj.RAIRT) || reload == 1
                if isempty(obj.TA_IN)
                    error('Requires Upper boudary condition file in the cfg file, STOP!!!');
                else
                    obj.RAIRT = dlmread([obj.TA_IN]);
                end
            end
            
            if isempty(obj.RSNOW) || reload == 1
                if isempty(obj.SD_IN)
                    warning('Missing snow depth file [SD_IN] in the cfg file, will set to 0 m');
                    obj.RSNOW = obj.RAIRT * 0.0;
                else
                    obj.RSNOW = dlmread([obj.SD_IN]);
                end
            end
            
            if isempty(obj.RHOSNOW) || reload == 1
                if isempty(obj.SR_IN)
                    warning('Missing snow density file [SR_IN] in the cfg file, will set to 200 kg/m3');
                    obj.RHOSNOW = obj.RAIRT * 0.0 + 200;
                else
                    obj.RHOSNOW = dlmread([obj.SR_IN]);
                end
            end
            
            if obj.SIM_TYPE == 0
                if isempty(obj.RDEWP) || reload == 1
                    if  ~isempty(obj.TDEW_IN)
                        obj.RDEWP = dlmread([obj.TDEW_IN]);
                    else
                        error('Requires Dewpoint [TDEW_IN] in the cfg file!');
                    end
                end
                
                if isempty(obj.RRADI) || reload == 1
                    if ~isempty(obj.RAD_IN)
                        obj.RRADI  = dlmread([obj.RAD_IN]);
                    else
                        
                        error('Requires Dewpoint [RAD_IN] in the cfg file!');
                    end
                end
                
                if isempty(obj.RWIND) || reload == 1
                    if  ~isempty(obj.WIND_IN)
                        obj.RWIND = dlmread([obj.WIND_IN]);
                    else
                        error('Requires Dewpoint [WIND_IN] in the cfg file!');
                    end
                end
                
                if isempty(obj.RPRE) || reload == 1
                    if  ~isempty(obj.PRES_IN)
                        obj.RPRE  = dlmread([obj.PRES_IN]);
                    else
                        error('Requires Dewpoint [PRES_IN] in the cfg file!');
                    end
                end
            end
            
            
            % Clip inputs:
            
            obj.RAIRT  = obj.RAIRT(1:obj.MN,end);
            obj.RSNOW  = obj.RSNOW(1:obj.MN,end);
            obj.RHOSNOW = obj.RHOSNOW(1:obj.MN,end);
            
            if obj.SIM_TYPE == 0
                obj.RDEWP  = obj.RDEWP(1:obj.MN,end);
                obj.RRADI   = obj.RRADI(1:obj.MN,end);
                obj.RWIND  = obj.RWIND(1:obj.MN, end);
                obj.RPRE   = obj.RPRE(1:obj.MN,end);
            end
            
            
        end
        
        function obj = load_geothermal_para(obj)
            
            % READ GEO_PAR
            
            if ~isempty(obj.GEOPAR_IN)
                
                data = dlmread([obj.GEOPAR_IN]);
                data = data(data(:,7)>0,:);
                
                obj.VWC   = data(:,1);
                obj.UWC_a = data(:,2);
                obj.UWC_b = data(:,3);
                obj.HC_a  = data(:,4);
                obj.HC_b  = data(:,5);
                obj.Ksoil = data(:,6);
                obj.Thick = data(:,7);
                obj.NSOIL_LAYER = size(obj.VWC,1);
                
            else
                error('Requires geothermal parameters! \n e.g. \n 1 \n 0.666	0.026	-0.38	-0.133300	6.255E-3	0.25	0.16',[]);
            end
            
        end
        
        function obj = load_grid_setup_file(obj)
            
            %% ===== READ Grid Setup =====
            
            if ~isempty(obj.VGRID_IN)
                
                data = dlmread([obj.VGRID_IN],'',0,0);
                
                if data(1)~=0
                    error(['The first node should be ZERO (0), please check [', obj.VGRID_IN, '] again']);
                end
                
            else
                
                warning('Missing grid setup file, creating default nodes');
                
                bottom_depth = sum(obj.Thick);
                
                data = 0:0.01:bottom_depth;
                
                if bottom_depth <= 15 && bottom_depth >2
                    
                    data = [0:0.01:1,...
                        1.02:0.02:2,...
                        2.1:0.1:bottom_depth];
                    
                end
                if bottom_depth <= 30 && bottom_depth >15
                    
                    data = [0:0.01:1,...
                        1.02:0.02:2,...
                        2.1:0.1:15,...
                        15.2:0.2:bottom_depth];
                    
                end
                
                if bottom_depth <= 50 && bottom_depth >30
                    
                    data = [0:0.01:1,...
                        1.02:0.02:2,...
                        2.1:0.1:15,...
                        15.2:0.2:30,...
                        30.5:0.5:bottom_depth];
                    
                end
                if bottom_depth > 50
                    
                    data = [0:0.01:1,...
                        1.02:0.02:2,...
                        2.1:0.1:15,...
                        15.2:0.2:30,...
                        30.5:0.5:50,...
                        51:bottom_depth];
                    
                end
                
                if data(end)<bottom_depth
                    
                    data = [data, bottom_depth];
                    
                end
            end

            obj.NSOIL_NODE = numel(data);
            obj.NODE       = obj.NSOIL_NODE + obj.NSNOW;
            
            obj.XYN(obj.NSNOW+1:obj.NODE,1) = data;
            
            % find observational depth index:
            
            if ~isempty(obj.n_obs_depth) && obj.n_obs_depth>=1
                
                obj.obs_data = nan(obj.MN, obj.n_obs_depth);
                
                for i = 1:obj.n_obs_depth
                    idx0 = find(abs(data - obj.obs_depth(i))<1E-5);
                    if ~isempty(idx0)
                        obj.obs_depth_index = [obj.obs_depth_index; idx0];
                        
                        try
                            obs0 = dlmread(obj.obs_files{i});
                            obs0 = obs0(1:obj.MN,end);
                            obj.obs_data(:,i) = obs0;
                        catch
                            warning(['Something wrong in loading observation at ', num2str(obj.obs_depth(i))]);
                            
                        end
                        
                    end
                end
                
            end
            
        end
        
        function obj = load_initial_temperature(obj)
            
            % READ INITIAL TEMPERATURE
            
            if ~isempty(obj.INITIAL_IN)
                %
                data  = dlmread([obj.INITIAL_IN]);
                data2 = interp1(data(:,1), data(:,2), obj.XYN(obj.NSNOW+1:end,1));
                %
            else
                
                TTOP = obj.calculate_TTOP();
                
                data2 = obj.XYN(obj.NSNOW+1:end,1) *0. + TTOP;
                
                warning(['Missing initial temperature file [INITIAL_IN], creating default by using uniform TTOP (',num2str(TTOP,'%0.2f'),' degC)']);
            end
            
            obj.RTT(obj.NSNOW+1:obj.NODE,1) = data2;
            
        end
        
        function ttop  = calculate_TTOP(obj)
            
            uts = PF_Utils;
            
            NYEAR = obj.MN / obj.NDAY;
            
            amp = obj.RAIRT;
            amp = reshape(amp, obj.NDAY, NYEAR);
            amp = mean(amp,2);
            
            t = 1:obj.NDAY;
            t = t';
            
            snd0 = obj.RSNOW;
            snd0(snd0<=0.005) = NaN;
            
            sden0 = obj.RHOSNOW;
            sden0(snd0<=0.005) = NaN;
            
            snd0 = reshape(snd0, obj.NDAY, NYEAR);
            snd0 = nanmean(snd0,2);
            snd0 = nanmean(snd0);
            
            sden0 = reshape(sden0, obj.NDAY, NYEAR);
            sden0 = nanmean(sden0,2);
            sden0 = nanmean(sden0);
            
            [DDT, DDF, ~] = uts.estimate_FN_GRND(t, amp, snd0, 100);
            
            ttop = obj.estimate_TTOP(DDT, DDF);            
            
            %             round(ttop_min, 2), round(ttop_max, 2)
        end
        
        function ttop_avg = estimate_TTOP(obj, DDT_gnd, DDF_gnd)
            
            rk = (0.56 / 2.24)^0.50;
            
            ttop_avg = -999;
            
            if rk*DDT_gnd - DDF_gnd <=0
                ttop_avg = round(1/(365)*(-rk*DDT_gnd - DDF_gnd),2);
            end
                        
        end
        
    end
end

