classdef PF_Utils
    
    % PF_Utils This class contains some functions that are used in
    % permafrost studies.
    
    methods
        
        function tgdata = quick_mean_annual_ground_temperature(obj, mod1, depth)
            
            tgdata = obj.quick_find_ground_temperature(mod1, depth);
            
            ncycle = mod1.MN / mod1.NDAY;
            
            tgdata = reshape(tgdata, mod1.NDAY, ncycle);
            
            tgdata = mean(tgdata,1);
            
            tgdata = tgdata';
            
        end
        
        function tgdata = quick_find_ground_temperature(obj, mod1, depth)
            
            ydepth = mod1.XYN(mod1.NSNOW+1:end);
            
            idx = find(abs(ydepth - depth)<1E-5);
            
            if ~isempty(idx)
                
                tgdata = mod1.O_TSOIL(:, idx);
                
            else
                
                idx_up = find(ydepth < depth, 1, 'last');
                idx_bt = find(ydepth > depth, 1, 'first');
                
                tgdata_up = mod1.O_TSOIL(:, idx_up);
                tgdata_bt = mod1.O_TSOIL(:, idx_bt);
                
                a = (depth - ydepth(idx_up))/(ydepth(idx_bt) - ydepth(idx_up));
                
                tgdata = (tgdata_bt - tgdata_up) .* a + tgdata_up;
                
            end
            
        end
        
        function quick_export_all(obj, mod1, cfg_out_file)
            
            obj_str_name = obj.getVarName(mod1);
            
            fold_out = strfind(cfg_out_file,filesep);
            if ~isempty(fold_out)
                path_out = cfg_out_file(1:fold_out(end));
                file_name_only = cfg_out_file(fold_out(end)+1:end);
            else
                path_out = pwd;
                file_name_only = cfg_out_file;
            end
            if ~exist(path_out,'dir')
                mkdir(path_out);
            end
            
            direct_list_on_cfg = {'MN','SEC_IN_STEP','ONSET_DATE_STRING',...
                'QQ','RHOWATER','RTF','GRAVITY','ZSNOW','EMISNOW','ASOIL',...
                'ZSOIL','EMISOIL','ROA','CPA','VONK','SIGMA','RLS','RZZH',...
                'SIM_TYPE','START_TYPE','n_obs_depth'};
            
            input_file_list = { 'TA_IN', 'RAIRT', 'tair';...
                'SD_IN', 'RSNOW', 'snod'; ...
                'SR_IN', 'RHOSNOW', 'sden';...
                'TDEW_IN','RDEWP','tdew';...
                'RAD_IN','RRADI','solar';...
                'WIND_IN','RWIND','wind';...
                'PRES_IN','RPRE','pres'};
            
            fid = fopen(cfg_out_file, 'wt');
            
            fprintf(fid, '%s\n', '%1 Part I: Basic configuration');
            
            for i = 1:numel(direct_list_on_cfg)
                eval(['va= ',obj_str_name,'.',direct_list_on_cfg{i},';']);
                if ~isempty(va)
                    if ~ischar(va)
                        if va<=1E-6
                            fprintf(fid,'%20s | %-e\n',direct_list_on_cfg{i}, va);
                        else
                            fprintf(fid,'%20s | %-f\n',direct_list_on_cfg{i}, va);
                        end
                    else
                        fprintf(fid,'%20s | %-64s\n',direct_list_on_cfg{i}, va);
                    end
                end
            end
            
            for i = 1:size(input_file_list,1)
                
                eval(['va= ',obj_str_name,'.',input_file_list{i,1},';']);
                
                if ~isempty(va)
                    
                    fprintf(fid,'%20s | %-64s\n',input_file_list{i,1}, [input_file_list{i,3}, '.dat']);
                    
                    eval(['va2= ',obj_str_name,'.',input_file_list{i,2},';']);
                    
                    writematrix(va2, [path_out, filesep, input_file_list{i,3}, '.dat']);
                    
                end
            end
            
            fprintf(fid, '%20s | %-64s\n', 'INITIAL_IN', 'initial.txt');
            eval(['va= ',obj_str_name,'.INITIAL_IN;'])
            if ~isempty(va)
                copyfile(va, [path_out, filesep, 'initial.txt']);
            end
            fprintf(fid, '%20s | %-64s\n', 'GEOPAR_IN' , 'geo.txt');
            new_par_table = [mod1.VWC mod1.UWC_a mod1.UWC_b mod1.HC_a mod1.HC_b mod1.Ksoil mod1.Thick];
            writematrix(new_par_table, [path_out, filesep, 'geo.txt']);
            
            fprintf(fid, '%20s | %-64s\n', 'VGRID_IN'  , 'grid.txt');
            eval(['va= ',obj_str_name,'.XYN(',obj_str_name,'.NSNOW+1:end);']);
            writematrix(va, [path_out, filesep, 'grid.txt']);
            
            
            fprintf(fid, '%s\n','               ');
            fprintf(fid, '%s\n','%==============');
            fprintf(fid, '%s\n','               ');
            
            eval(['n_obs_depth = ',obj_str_name,'.n_obs_depth;']);
            eval(['obs_depth = ',obj_str_name,'.obs_depth;']);
            
            if ~isempty(n_obs_depth) && n_obs_depth >=1
                
                fprintf(fid, '%s\n', '%2 Part II: Observation');
                
                for iii = 1:n_obs_depth
                    
                    filename = ['tg', num2str(obs_depth(iii),'%0.2f'),'m.txt'];
                    
                    fprintf(fid, '%20f | %-64s\n', obs_depth(iii), filename);
                    
                    eval(['obs0=', obj_str_name, '.obs_data(:,', num2str(iii,'%d'),');']);
                    
                    writematrix(obs0, [path_out, filesep, 'tg', num2str(obs_depth(iii),'%0.2f'),'m.txt']);
                    
                end
                
            end
            
            fclose(fid);
            
            fid = fopen([path_out, filesep, 'test.m'],'wt');
            fprintf(fid, '%-s\n','clc;clear;close all;');
            fprintf(fid, '%-s\n','mod1 = PF_Core;');
            fprintf(fid, '%-s\n','uts = PF_Utils;');
            fprintf(fid, '%-s\n',['mod1 = uts.load_cfg_file(''',file_name_only,''',mod1);']);
            fprintf(fid, '%-s\n','mod1 = mod1.initialize();');
            fprintf(fid, '%-s\n','for i=1:mod1.MN');
            fprintf(fid, '%-s\n','mod1 = mod1.update(i);');
            fprintf(fid, '%-s\n','end');
            fprintf(fid, '%-s\n','uts.quick_plot_last_envelope(mod1);');
            
            fclose(fid);
            
        end
        
        function h = quick_ground_temperature_field(obj, mod1, bottom)
            
            % This function is to plot ground temperature field up to
            % bottom dept.
            
            h = figure;
            
            [xtime,~] = obj.quick_create_date_axis(mod1);
            idx = find(mod1.XYN(mod1.NSNOW+1:end)<=bottom);
            data = mod1.O_TSOIL(:,idx);
            ydepth = mod1.XYN(mod1.NSNOW+1:end); ydepth = ydepth(idx);
            
            cmap = obj.rescale_cmap(0, floor(min(data(:)))-1, floor(max(data(:)))+1);
            colormap(cmap);
            contourf(xtime', ydepth, data');
            colorbar();
            caxis([floor(min(data(:)))-1, floor(max(data(:)))+1]);
            axis ij
            datetick('x','mm','keeplimits');
            
        end
        
        
        function [x_all, x_unit] = quick_create_date_axis(obj, mod1)
            
            date_onset = mod1.ONSET_DATE_STRING;
            length     = mod1.MN;
            date_step  = mod1.SEC_IN_STEP;
            
            x_all = 1:mod1.MN;
            
            x_unit  = 'unitless';
            
            if date_step == 86400
                
                % day========
                x_unit = 'day';
                
                date0 = datenum(date_onset):(datenum(date_onset)+length-1);
                date1 = datevec(date0);
                
                %check feb-29
                idx_feb_29 = find(date1(:,2)==2 & date1(:,3)==29);
                n_feb_29 = numel(idx_feb_29);
                
                length = length + n_feb_29;
                
                date0 = datenum(date_onset):(datenum(date_onset)+length-1);
                date0(idx_feb_29) = [];
                x_all = date0;
                
            end
            
            if date_step == 365*86400/12
                
                %month========
                
                x_unit = 'month';
                
                date0 = datenum(date_onset);
                date1 = datevec(date0);
                
                date2 = datenum(date1(1), 1:length, 15);
                
                x_all = date2;
                
            end
            
        end
        
        function quick_write_geo_parameters(obj, mod1, file0)
            
            new_par_table = [mod1.VWC mod1.UWC_a mod1.UWC_b mod1.HC_a mod1.HC_b mod1.Ksoil mod1.Thick];
            
            writematrix(new_par_table, file0);
            
        end
        
        function alt = quick_estimate_ALT(obj, depth_all, temp_max, max_front_depth)
            
            if  nargin <=3
                max_front_depth = 10;
            end
            
            alt = NaN;
            
            %find max thawed or frozen depth:
            if min(temp_max)<0 && max(temp_max)>0
                bbb = 0:0.01:max_front_depth;
                aaa = interp1(depth_all, temp_max, bbb);
                [~, alt] = min(abs(aaa));
                alt = bbb(alt(1));
            end
        end
        
        function h = quick_plot_last_envelope(obj, mod1, bottom_depth)
            
            if  nargin <=2
                bottom_depth = 15;
            end
            
            depth_all = mod1.XYN((mod1.NSNOW+1):end);
            temp_all = mod1.O_TSOIL(mod1.MN-mod1.NDAY+1:mod1.MN,:);
            
            idx0 = find(depth_all<=bottom_depth);
            
            depth_all = depth_all(idx0);
            temp_all  = temp_all(:, idx0);
            
            max_tmp = max([temp_all(:)]);
            min_tmp = min([temp_all(:)]);
            
            temp_max  = max(temp_all,[],1);
            temp_min  = min(temp_all,[],1);
            temp_all  = mean(temp_all,1);
            
            alt = obj.quick_estimate_ALT(depth_all, temp_max);
            
            
            %======================
            
            h = figure;
            
            plot(temp_min, depth_all, 'b', 'linew', 2);hold on;
            plot(temp_all, depth_all, 'k', 'linew', 2);
            plot(temp_max, depth_all, 'r', 'linew', 2);
            
            if max_tmp*min_tmp<0
                plot(depth_all*0, depth_all,'k--');
            end
            
            if ~isnan(alt)
                
                scatter(0, alt,'ro','filled');
                
                text(0, alt*1.2, num2str(alt,'%0.2f'));
                
            end
            
            legend('Min','Mean','Max','location','southwest');
            
            xlabel('Temperature (^oC)');
            ylabel('Depth (m)');
            
            axis ij;
            
            obj.set_axis();
            
            pbaspect([1 1.618 1]);
            
        end
        
        function quick_write_temp_profile(obj, mod2, file0, bot0)
            
            if  nargin <3
                idx = find(mod2.XYN<=bot0);
            else
                idx = numel(mod2.XYN);
            end
            
            data = [mod2.XYN(mod2.NSNOW+1:idx), mod2.RTT(mod2.NSNOW+1:idx)];
            
            writematrix(data, file0);
            
        end
        
        function f0 = quick_compare_obs(obj, mod2, ncol)
            
            if  nargin <=2
                ncol = 1;
            end
            
            if mod(mod2.n_obs_depth , ncol)==0
                nrow = mod2.n_obs_depth / ncol;
            else
                nrow = floor(mod2.n_obs_depth / ncol) + 1;
            end
            
            [x_date, x_unit] = obj.quick_create_date_axis(mod2);
            
            f0 = figure;
            
            for i = 1:mod2.n_obs_depth
                
                depth0 = mod2.obs_depth(i);
                
                sim = mod2.O_TSOIL(1:mod2.MN, mod2.obs_depth_index(i));
                obs = mod2.obs_data(1:mod2.MN, i);
                
                subplot(nrow, ncol, i);
                
                scatter(x_date, obs,'filled'); box on; hold on;
                plot(x_date, sim,'linew',2);
                
                if ~strcmpi(x_unit, 'unitless')
                    datetick('x','yym','keepticks');
                end
                
                xlim([min(x_date), max(x_date)]);
                
                if i ==1
                    ylim0 = [min([sim;obs]), max([sim;obs])]*1.01;
                end
                
                cmap = obj.rescale_cmap(0,ylim0(1),ylim0(2));
                colormap(cmap);
                
                ylim(ylim0);
                
                if prod(ylim0)<0
                    plot(x_date, x_date*0, 'k--', 'linew', 1);
                end
                
                if i == 1
                    legend('obs','sim');
                end
                
                rmse0 = obj.RMSE(sim, obs);
                nash0 = obj.NASH(sim, obs);
                
                title([num2str(depth0,'%0.2f'),'m [', num2str(rmse0,'%0.2f'),'],[',num2str(nash0,'%0.2f'),']']);
                
                obj.set_axis();
                
                pbaspect([1.618 1 1]);
                
            end
            
        end
        
        function set_axis(obj)
            
            set(gca,'linewidth',1.5);
            set(gca,'fontsize',13);
            set(gca,'xminortick','on');
            set(gca,'yminortick','on');
            set(gca,'tickdir','out');
            set(gca,'ticklength',[0.02,0.03]);
            % set(gca,'xticklabel',sprintf('%d\n',get(gca,'xtick')));
            % set(gca,'yticklabel',sprintf('%d\n',get(gca,'ytick')));
            set(gcf, 'InvertHardCopy', 'off');set(gcf,'color',[1,1,1]);
            set(gca,'color',[0.95,0.95,0.95]);
            
        end
        
        function set_figure_output_size(obj, h, papersize)
            
            if  nargin ==2
                papersize= [15 8];
            end
            
            position = [0 0 papersize(1) papersize(2)];
            set(h,'paperunits','centimeters');
            set(h,'PaperSize',papersize)
            set(h,'paperposition',position);
            
        end
        
        function cmd0 = pass_value_to_model(obj,st0, obj_name_str)
            
            st0 = strsplit(st0,'|');
            if ~isnan(str2double(st0{2}))
                cmd0 = [obj_name_str,'.',strtrim(st0{1}),'=',strtrim(st0{2}),';'];
            else
                cmd0 = [obj_name_str,'.',strtrim(st0{1}),'=''',strtrim(st0{2}),''';'];
            end
        end
        
        function s = getVarName(obj, a)
            s = inputname(2);
        end
        
        function mod1 = load_cfg_file(obj, cfg_file, mod1)
            
            obj_name_str = obj.getVarName(mod1);
            
            fid = fopen(cfg_file);
            
            while ~feof(fid)
                
                st0 = fgetl(fid);
                st0 =strtrim(st0);
                
                if ~isempty(strtrim(st0))
                    
                    if st0(1) == '%'
                        
                        switch st0(2)
                            
                            case '1'
                                for i =1:20000
                                    st0 = fgetl(fid);
                                    if ~isempty(strtrim(st0)) && st0(1) ~= '%'
                                        cmd0 = obj.pass_value_to_model(st0, obj_name_str);
                                        eval(cmd0);
                                    else
                                        break
                                    end
                                end
                            case '2'
                                eval(['n_depth =', obj_name_str,'.n_obs_depth;']);
                                if ~isempty(n_depth) && n_depth>=1
                                    for i =1:n_depth
                                        str0 = fgetl(fid);
                                        str0 = strsplit(str0, '|');
                                        
                                        dp0 = strtrim(str0{1});
                                        file0 = strtrim(str0{2});
                                        
                                        eval([obj_name_str, '.obs_depth=[',obj_name_str, '.obs_depth,', dp0,'];']);
                                        eval([obj_name_str, '.obs_files=[',obj_name_str, '.obs_files;''', file0,'''];']);
                                    end
                                end
                            otherwise
                                
                        end
                        
                    end
                    
                end
                
            end
            
            fclose(fid);
            
        end
        
        function [tavg, prec, time] = get_TerraClimate_data(obj, latList, lonList, year_start, month_start, year_end, month_end)
            
            % Download point data from TerraClimate using THREDDS web services
            % I - latList: objective latitude
            % I - lonList: objective longitude
            % I - year_start: onset year
            % I - month_start: onset month in the onset year
            % I - year_end: last year
            % I - month_end: last month in the last year
            % O - tavg : monthly mean air temperature (degree C)
            % O - prec : monthly total precipitation (mm)
            %
            % See details of the TerraClimate Dataset:
            %    Abatzoglou, J. T., Dobrowski, S. Z., Parks, S. A., & Hegewisch, K. C. (2018).
            %       TerraClimate, a high-resolution global dataset of monthly climate and climatic
            %       water balance from 1958?2015. Scientific data, 5,
            %       170191. DOI: 10.1038/sdata.2017.191
            
            VarList = {'tmin','tmax','ppt'};
            
            for ivar = 1:3
                
                myVar = VarList{ivar};
                
                %enter your terraclimate OPeNDAP URL
                %find this here: http://thredds.northwestknowledge.net:8080/thredds/terraclimate_aggregated.html
                myURL = ['http://thredds.northwestknowledge.net:8080/thredds/dodsC/agg_terraclimate_',myVar,'_1958_CurrentYear_GLOBE.nc'];
                
                %===================================================
                timebounds_start =datenum(year_start,month_start,1)-datenum(1900,1,1);
                timebounds_end = datenum(year_end,month_end,1)-datenum(1900,1,1);
                timebounds = [timebounds_start timebounds_end];
                lat=ncread(myURL,'lat');
                lon=ncread(myURL,'lon');
                
                time=ncread(myURL,'time');
                [Y,M,D]=datevec(time+datenum(1900,1,1));
                ftimei=find(time>=timebounds(1) & time<=timebounds(2));
                time = datenum(Y(ftimei), M(ftimei), D(ftimei));
                
                [~   ,flati]=min(abs(lat-latList(1)));
                [~   ,floni]=min(abs(lon-lonList(1)));
                
                % to change variables, see list here: http://thredds.northwestknowledge.net:8080/thredds/terraclimate_aggregated.html
                %dimensions of the data should be:
                
                myData=ncread(myURL,myVar,[floni(1) flati(1) ftimei(1)],[length(floni) length(flati) length(ftimei)],[1 1 1]);
                
                switch myVar
                    case 'tmin'
                        tmin = myData(:);
                    case 'tmax'
                        tmax = myData(:);
                    case 'ppt'
                        prec = myData(:);
                    otherwise
                        break
                end
                
            end
            
            tavg = (tmin + tmax) * 0.5;
            
        end
        
        
        
        function [snod, sden] = run_simple_snow(obj, tair, prec, ...
                old_snow_depth, old_density, icl, iopen, padj,sixhrs)
            
            % Run snow model using the empirical algorithm for snow depth and snow density.
            %
            %     I    - old_snow_depth    - ORIGINAL SNOW DEPTH FIELD (cm)
            %     I    - tair              - Surface Temperatures (deg.C)
            %     I    - prec              - PRECIPITATION FIELD (m)
            %     I    - old_density       - INITIAL MEAN DENSITY OF SNOW PACK IN KG/M3
            %     I    - icl               - SNOW CLIMATE CLASS, STURM ET AL 1995 (CODE FROM 0-7)
            %     I    - iopen             - FORESTED/OPEN FLAG FOR VALIDATING RESULTS IN BOREAL
            %                                FOREST ZONE (MOST SNOW DEPTHS MEASURED AT OPEN SITES)
            %                                IOPEN=1 IN OPEN, O IN FOREST
            %     O    - snod              - SIMULATED SNOW DEPTH (cm)
            %     O    - sden              - SIMULATED DENSITY OF SNOW PACK (KG/M3)
            
            
            n_time_steps = numel(tair);
            snod = nan(n_time_steps,1);
            sden = nan(n_time_steps,1);
            
            save_OLD = old_snow_depth;
            save_CD  = old_density;
            
            for i = 1:n_time_steps
                
                t2_air = tair(i);
                
                if i == 1
                    t2_last = tair(i) ;
                else
                    t2_last = tair(i-1);
                end
                
                old    = save_OLD;
                cd     = save_CD;
                
                ts1    = t2_last; % previous temperature
                ts2    = t2_air; % current temperature
                pcpn   = prec(i); % mm
                
                [new, newp] = obj.new_bruce(old,ts1,ts2,pcpn,cd,icl,iopen,padj,sixhrs);
                
                save_OLD = new;
                save_CD  = newp;
                
                snod(i) = new;
                sden(i) = newp;
                
            end
        end
        
        function [new, newp] = new_bruce(obj, old,ts1,ts2,pcpn,cd,icl,iopen,padj,sixhrs)
            
            % Empirical algorithm for snow depth and snow density.
            %
            % * Changes (2019-03-29) remove TSFC variable, use TS1 and TS2 only.
            % *
            % *AUTHOR  - B. BRASNETT  FEBRUARY 1997
            % *
            % *PURPOSE - EMPIRICAL ALGORITHM TO MELT SNOW ACCORDING TO THE
            % *          SURFACE TEMPERATURE AND INCREASE SNOW DEPTH ACCORDING
            % *          TO THE PRECIPITATION THAT HAS FALLEN SINCE THE LAST
            % *          ANALYSIS TIME
            % *
            % *ARGUMENTS-
            % *   I/O   - OLD    - ORIGINAL SNOW DEPTH FIELD (cm)
            % *    O    - NEW    - MODIFIED SNOW DEPTH FIELD (cm)
            % *    I    - TS1    - Surface Temperature in Last Time Step (deg.C)
            % *    I    - TS2    - Current Surface Temperature (deg.C)
            % *    I    - PCPN   - PRECIPITATION FIELD (m)
            % *   I/O   - CD     - INITIAL MEAN DENSITY OF SNOW PACK IN KG/M3
            % *    O    - NEWP   - FINAL MEAN DENSITY OF SNOW PACK IN KG/M3
            % *    I    - ICL    - SNOW CLIMATE CLASS, STURM ET AL 1995 (CODE FROM 0-7)
            % *    I    - IOPEN  - FORESTED/OPEN FLAG FOR VALIDATING RESULTS IN BOREAL
            % *                    FOREST ZONE (MOST SNOW DEPTHS MEASURED AT OPEN SITES)
            % *                    IOPEN=1 IN OPEN, O IN FOREST
            % *
            % c...Sturm Snow Classification:
            % c
            % c  		water = 0            RHOMIN
            % c  		tundra snow = 1      200
            % c  		taiga snow = 2       160
            % c  		maritime snow = 3    160
            % c  		ephemeral snow = 4   180
            % c  		prairie snow = 5     140
            % c  		alpine snow = 6      120
            % c  		ice = 7 (ice caps)   (same as Tundra)
            % c
            
            
            rhow   = 1000.;
            rhoice = 917.;
            cw     = 4.18e3;
            lf     = 3.34e5;
            
            rhomin = [200, 160, 160, 180, 140, 120, 200];
            rhomax = 600;
            
            tmelt = -1.;  %melt threshold temp
            sixth = 1./(sixhrs);
            delt = 3600.; %1-hour time step (s)
            
            % sfall = 0;
            
            %_____screen out grid points with no snow and no possiblity of getting
            %     snow over the period
            %
            if ~((old <= 0. && ts1 > 2. && ts2 > 2.))
                %
                %_____convert snow volume to water equivalent (mm)
                %
                density = cd;
                density = max(rhomin(icl),min(rhomax,density));
                h2o = old*density*0.01; %snow water equiv (mm)
                
                %...determine melt factor (gamma) as a function of snow density (age)
                %   and vegetation type following kuusisto (1980)
                %
                if icl==2 && iopen == 0 %boreal forest
                    %	   dd = 0.0104 * density - 0.70
                    dd = 0.0104/2. * density - 0.70; %reduce melt index by half
                    %	   if(dd.lt.1.4) dd = 1.4
                    if dd < 0.1
                        dd = 0.1;
                    end
                    if dd > 3.5
                        dd = 3.5;
                    end
                    pcpn = 0.8*pcpn; %reduce precip 20% for canopy interception/sublimation
                else
                    %	   dd = 0.0196 * density - 2.39	!open areas
                    dd = 0.0196/2. * density - 2.39;	%open areas
                    %	   if(dd.lt.1.5) dd = 1.5
                    if dd < 0.1
                        dd = 0.1;
                    end
                    if dd > 5.5
                        dd = 5.5;
                    end
                end
                
                gamma = dd/24.; %convert from mm/day to mm/hr
                
                %...reduce tundra and prairie snowpack precip 20% for blowing snow
                %   sublimation loss (low vegetation, exposed to wind)
                %
                if icl == 1 || icl == 5
                    pcpn = padj * pcpn;
                end
                %_______for each timestep, melting algorithm first, then
                %       densification due to melting, then add snowfall accumulation
                %       adjust density for new snow after the final timestep
                %
                new = h2o;
                %
                %...compute new snowfall density as function of air temperature following
                %   hedstrom and pomeroy (1998).  for temperatures greater than 0c assume
                %   a linear relationship between snow density and temp following
                %   pomeroy and gray (1995) fig. 1 to max of 200 kg.m-3. (rb 30/09/98)
                %
                if ts1<=0
                    rhosfall = 67.9 + 51.3*exp(ts1/2.6);
                else
                    rhosfall = 119.2 + 20.*ts1;
                    if rhosfall > 200.
                        rhosfall = 200.;
                    end
                end
                
                %
                %_______precipitation has units of metres
                %       convert metres to millimeters (i.e. kg/m2) before using the pcpn
                %
                %
                %...determine precip phase.  precip is the accumulated total (m) over 6 hrs.
                %   do not treat liquid water storage/refreeze in snowpack.
                %
                snow = 0.;
                rain = 0.;
                if pcpn > 0.
                    if ts1 > 0.
                        phase = 0.;
                    else
                        phase = 1.;
                    end
                    
                    %
                    %...evaluate the inclusion of mixed precip
                    %
                    snow = pcpn*phase;
                    rain = pcpn*(1.-phase);
                    
                    
                end
                if snow > 0.0
                    
                    sfall = 0.5*sixth*1000.*max(0.0,snow);
                    
                    if sfall > 0.
                        
                        new     = new + sfall; %accumulate new snow water equiv
                        fresh   = max(0.0,sfall);
                        newdens = (rhosfall*fresh + density*(new - fresh))/new; %adjust density for new snow
                        density = max(newdens,rhomin(icl));
                        
                    end
                end
                
                %
                %...take any rain melt into effect
                %
                if  rain > 0. && ts1 > 0.
                    prain = rain*1000./(6.*3600.); %convert rain back to kg/m2/s
                    rmelt = (rhow*cw*prain*ts1)/(lf*rhoice);  % kg/m2/s (mm/s)
                    rmelt = rmelt/1000. * (6.*3600.); % m
                    new = max(0.0,new - 0.5*sixth*1000.*rmelt);
                end
                
                %_________melt at temperature ts1 (weighted by 0.5)
                %
                tdd = ts1 - tmelt;
                if tdd > 0.0
                    new = max(0.0, new - 0.5*gamma*tdd);
                end
                
                
                %...age snow
                %
                %..warm wet snow - use higher upper limit for settling. value of 700 selected
                %  based on performance at col de porte.
                %
                if ts1 >= tmelt && new > 0.
                    sdepcm = new/density * 100.;
                    denmax = 700. - (20470./sdepcm)*(1.-exp(-sdepcm/67.3));
                    %	    write(66,*) density,denmax
                    den_diff = denmax - density;
                    if den_diff>0.1
                        timfac=exp(log((denmax-density)/200.0)-2.778e-6*(delt/2.));%delt divided by 2 for first and last time steps
                        rhonew=denmax-200.0*timfac;
                        density=min(rhomax,rhonew);
                    else
                        density = min(rhomax,density);
                    end
                end
                
                
                %...cold snow aging:
                %
                if ts1 < tmelt && new > 0.
                    %
                    if icl == 2 || icl == 6 %cold snowpacks (depth hoar)
                        c2 = -28.;
                    else
                        c2 = -21.;
                    end
                    %
                    %...snow settling as a function of swe and temperature following
                    %   anderson (1978). bulk swe is multiplied by factor of 0.6 to approximate
                    %   computing over entire snow column.  different values of c2 used (28 vs 21)
                    %   for deep snowpacks.
                    %
                    den_gcm = density/1000.;
                    del_den = 0.02*exp(-0.08*(tmelt-ts1))* 0.6*new/10.*exp(c2*den_gcm);
                    dgain = del_den*1000.; %convert to kg.m-3
                    rhonew = density + dgain/2.;
                    density=min(rhomax,rhonew);
                end
                
                for time = 1:sixhrs-1
                    
                    ts = ((ts2 - ts1)*(time)*sixth + ts1);
                    
                    %...compute new snowfall density as function of air temperature following
                    %   hedstrom and pomeroy (1998)
                    %
                    if ts <= 0
                        rhosfall = 67.9 + 51.3*exp(ts/2.6);
                    else
                        rhosfall = 119.2 + 20.*ts;
                        if rhosfall > 200.
                            rhosfall = 200.;
                        end
                    end
                    
                    %...determine precip phase, add new snowfall and adjust density
                    %
                    snow = 0.;
                    rain = 0.;
                    
                    if pcpn > 0.
                        
                        if ts>0.
                            phase = 0.;
                        else
                            phase = 1.;
                        end
                        %
                        %...evaluate the inclusion of mixed precip
                        %
                        %	     if(ts.ge.2.) phase = 0.
                        %	     if(ts.le.0.) phase = 1.
                        %	     if(ts.gt.0. .and. ts.lt.2.) then
                        %	        phase = 1.0 - ts*0.5
                        %	     endif
                        
                        snow = pcpn*phase;
                        rain = pcpn*(1.-phase);
                    end
                    
                    if snow > 0.0
                        sfall = sixth*1000.*max(0.0,snow);
                        if sfall > 0.
                            new = new + sfall; %accumulate new snow water equiv
                            fresh = max(0.0,sfall);
                            newdens = (rhosfall*fresh + density*(new - fresh))/new; %adjust density for new snow
                            density = max(newdens,rhomin(icl));
                        end
                    end
                    
                    %...take any rain melt into effect.
                    %
                    if  rain > 0. &&  ts > 0.
                        prain = rain*1000./(6.*3600.); %convert rain back to kg/m2/s
                        rmelt = (rhow*cw*prain*ts)/(lf*rhoice);  % kg/m2/s (mm/s)
                        rmelt = rmelt/1000. * (6.*3600.); % m
                        new = max(0.0,new - sixth*1000.*rmelt);
                    end
                    
                    %___________melt at temperature ts
                    %
                    tdd = ts - tmelt;
                    if  tdd > 0.0
                        new = max(0.0, new - gamma*tdd);
                    end
                    %_________densification due to melt-freeze metamorphism
                    %
                    %
                    %..warm wet snow - use higher upper limit for settling. value of 700 selected
                    %  based on performance at col de porte.
                    %
                    if ts >= tmelt && new > 0.
                        sdepcm = new/density * 100.;
                        denmax = 700. - (20470./sdepcm)*(1.-exp(-sdepcm/67.3));
                        %	   write(66,*) density,denmax
                        den_diff = denmax - density;
                        if den_diff > 0.1
                            timfac=exp(log((denmax-density)/200.0)-2.778e-6*delt);
                            rhonew=denmax-200.0*timfac;
                            density=min(rhomax,rhonew);
                        else
                            density = min(rhomax,density);
                        end
                    end
                    
                    %...cold snow aging:
                    %
                    if ts < tmelt && new > 0.
                        %
                        if icl == 2 || icl == 6  %deep cold snowpacks
                            c2 = -28.;
                        else
                            c2 = -21.;
                        end
                        
                        %...snow settling as a function of swe and temperature following
                        %   anderson (1978). bulk swe is multiplied by factor of 0.6 to approximate
                        %   computing over entire snow column. different values of c2 used (28 vs 21)
                        %   for deep snowpacks.
                        %
                        den_gcm = density/1000.;
                        del_den = 0.02*exp(-0.08*(tmelt-ts))*  0.6*new/10.*exp(c2*den_gcm);
                        dgain = del_den*1000.; %convert to kg.m-3
                        rhonew = density + dgain;
                        density=min(rhomax,rhonew);
                    end
                end
                %-----------------------------------------------------------------------------------
                %_________conditions for ts2 (weighted by 0.5)
                %
                %...compute new snowfall density as function of air temperature following
                %   hedstrom and pomeroy (1998)
                %
                if ts2 <= 0
                    rhosfall = 67.9 + 51.3*exp(ts2/2.6);
                else
                    rhosfall = 119.2 + 20.*ts2;
                    if rhosfall > 200.
                        rhosfall = 200.;
                    end
                end
                
                %...determine precip phase, add new snowfall and adjust density
                %
                snow = 0.;
                rain = 0.;
                
                if pcpn > 0.
                    
                    if ts2 > 0.
                        phase = 0.;
                    else
                        phase = 1.;
                    end
                    %
                    %...evaluate the inclusion of mixed precip
                    %
                    %	     if(ts2.ge.2.) phase = 0.
                    %	     if(ts2.le.0.) phase = 1.
                    %	     if(ts2.gt.0. .and. ts2.lt.2.) then
                    %	        phase = 1.0 - ts2*0.5
                    %	     endif
                    
                    snow = pcpn*phase;
                    rain = pcpn*(1.-phase);
                end
                
                if snow > 0.0
                    sfall = 0.5*sixth*1000.*max(0.0,snow);
                    if sfall>0.
                        new = new + sfall; %accumulate new snow water equiv
                        fresh = max(0.0,sfall);
                        newdens = (rhosfall*fresh + density*(new - fresh))/new; %adjust density for new snow
                        density = max(newdens,rhomin(icl));
                    end
                end
                
                %..take any rain melt into account
                %
                if rain > 0.
                    prain = rain*1000./(6.*3600.); %convert rain back to kg/m2/s
                    rmelt = (rhow*cw*prain*ts2)/(lf*rhoice);  % kg/m2/s (mm/s)
                    rmelt = rmelt/1000. * (6.*3600.); % m
                    new = max(0.0,new - 0.5*sixth*1000.*rmelt);
                end
                %...melt at tsfc(2)
                %
                tdd = ts2 - tmelt;
                if tdd > 0.0
                    new = max(0.0, new - 0.5*gamma*tdd);
                end
                
                %...warm snow aging
                %
                if ts2 >= tmelt && new > 0.
                    sdepcm = new/density * 100.;
                    denmax = 700. - (20470./sdepcm)*(1.-exp(-sdepcm/67.3));
                    %	    write(66,*) density,denmax
                    den_diff = denmax - density;
                    if den_diff>0.1
                        timfac=exp(log((denmax-density)/200.0)-2.778e-6*(delt/2.));
                        rhonew=denmax-200.0*timfac;
                        density=min(rhomax,rhonew);
                    else
                        density = min(rhomax,density);
                    end
                end
                
                %...cold snow aging
                %
                if ts2 < tmelt && new > 0.
                    %
                    if icl == 2 || icl == 6  %deep cold snowpacks
                        c2 = -28.;
                    else
                        c2 = -21.;
                    end
                    %
                    %...snow settling as a function of swe and temperature following
                    %   anderson (1978). bulk swe is multiplied by factor of 0.6 to approximate
                    %   computing over entire snow column.  different values of c2 used (28 vs 21)
                    %   for deep snowpacks.
                    %
                    den_gcm = density/1000.;
                    del_den = 0.02*exp(-0.08*(tmelt-ts2))*0.6*new/10.*exp(c2*den_gcm);
                    dgain = del_den*1000.; %convert to kg.m-3
                    rhonew = density + dgain/2.;
                    density=min(rhomax,rhonew);
                end
                
                %_______convert from water equivalent to depth
                %
                new  = 100.0*new/density;
                newp = density;
                new  = min(new, 600.); %600 cm max
                
            else
                
                new = 0.0;
                newp = rhomin(icl);
            end
            
        end
        
        function NSEs = NASH(obj, sim, obs)
            idx = find(~isnan(sim+obs));
            sim = sim(idx);
            obs = obs(idx);
            E0 = sim - obs;
            SSE0 = sum(E0.^2);
            ue0 = mean(obs);
            SSU0 = sum((obs- ue0).^2);
            NSEs = 1 - SSE0 / SSU0;
            if NSEs < 0
                NSEs = 0.01;
            end
        end
        
        function RMSEs = RMSE(obj,sim, obs)
            
            % RMSE It's used to calculate RMSE between the inputs.
            %
            % It will ignore the pairs when there is any missing value in any input variable.
            %
            %  INPUT:
            %         sim [n] : variable 1
            %         obs [n] : variable 2
            %  OUTPUT:
            %         RMSEs [1] : root-mean-squrare-errors between variable
            %                     1 and 2
            %  EXAMPLE:
            %      a = RMSE(rand(10,1), rand(10,1));
            %%
            
            idx = find(~isnan(sim+obs));
            sim = sim(idx);
            obs = obs(idx);
            RMSEs = sqrt(sum((sim - obs).^2)/(numel(idx)-1));
            
        end
        
        function para = fit_sin_cycle(obj,t, tmp)
            
            % fit_sin_cycle It's used to fit input temperature data to a cosine function.
            %
            %  INPUT:
            %         t   [n]: time
            %         tmp [n]: temperature
            %  OUTPUT:
            %         para [3]: para(1)-mean
            %                   para(2)-amplitude
            %                   para(3)-phase offset
            %  EXAMPLE:
            %      para = fit_sin_cycle(1:365, rand(365,1));
            %
            
            t = reshape(t, numel(t),1);
            tmp = reshape(tmp, numel(tmp),1);
            
            options = optimoptions('lsqcurvefit','Diagnostics','off',...
                'Display','off');
            
            x0 = [-5, 15, pi];
            fun = @(x, t) x(1)+x(2)*cos(2*pi*t/numel(t)+x(3));
            para = lsqcurvefit(fun, x0, t, tmp, [-90,0,-90],[90,90,90], options);
            
        end
        
        function [DDT_air, DDF_air, FN_air, Tavg, Aamp, beta, Lwin] = estimate_FN_air(obj,t, tmp)
            
            % estimate_FN_air It's used to estimate Air Freezing/Thawing Index from temperature input.
            %
            %  INPUT:
            %         t   [n]: time
            %         tmp [n]: temperature
            %  OUTPUT:
            %         DDT_air[1]: Air Thawing Index
            %         DDF_air[1]: Air Freezing Index
            %         FN_air [1]: Air Frost Number Index
            %         Tavg   [1]: Mean Annual Air Temperature
            %         Aamp   [1]: Annual Amplitude
            %         beta   [1]: Angle from thaw to frozen
            %         Lwin   [1]: length of cold season
            %
            %  EXAMPLES:
            %      [DDT_air, DDF_air, FN_air, Tavg, Aamp, beta, Lwin] = estimate_FN_air(1:365, rand(365,1));
            %      [DDT_air, DDF_air, FN_air]                         = estimate_FN_air(1:365, rand(365,1));
            %
            
            param = obj.fit_sin_cycle(t, tmp);
            
            beta = acos(-param(1)/param(2));
            
            Tsum = param(1) + param(2) / beta * sin(beta);
            Twin = param(1) - param(2) * (sin(beta)/ (pi - beta));
            
            Lsum = 365 * (beta / pi);
            Lwin = 365  - Lsum;
            
            DDT_air = Tsum * Lsum;
            DDF_air = -Twin * Lwin;
            
            FN_air  = sqrt(DDF_air) / (sqrt(DDT_air)+sqrt(DDF_air));
            
            Tavg = param(1);
            Aamp = param(2);
            
        end
        
        function [DDT_GRND, DDF_GRND, FN_GRND] = estimate_FN_GRND(obj,t, tmp, snd, sden)
            
            % estimate_FN_GRND It's used to estimate Ground Surface Freezing/Thawing Index.
            %                  from temperature and snow cover input. It
            %                  uses the [estimate_FN_air] function inside.
            %
            %  INPUT:
            %         t   [n]: time
            %         tmp [n]: temperature (C)
            %         snd [1]: winter-averaged snow thickness (m)
            %         sden[1]: winter-averaged snow density (kg/m3)
            %  OUTPUT:
            %         DDT_GRND[1]: Ground Surface Thawing Index
            %         DDF_GRND[1]: Ground Surface Freezing Index
            %         FN_GRND [1]: Ground Surface Frost Number Index
            %
            %  EXAMPLE:
            %      [DDT_GRND, DDF_GRND, FN_GRND] = estimate_FN_GRND(1:365, rand(365,1), 0.15, 180);
            %
            
            [DDT_air, DDF_air, FN_air, Tavg, Aamp, beta, Lwin] = estimate_FN_air(obj,t, tmp);
            
            if snd > 0
                
                ksnow = 2.9E-6 * sden^2;
                
                csnow = 2090 * sden;
                
                alpha_snow = ksnow / (csnow * sden);
                
                Z_star = sqrt(alpha_snow * 365/pi);
                
                A_plus= Aamp * exp(-snd / Z_star);
                
                DDT_GRND = DDT_air;
                
                Twin_plus = Tavg - A_plus * sin(beta)/(pi - beta);
                
                DDF_GRND = -Lwin *Twin_plus;
                
                FN_GRND = sqrt(DDF_GRND) / (sqrt(DDT_GRND)+sqrt(DDF_GRND));
                
            else
                
                DDT_GRND = DDT_air;
                DDF_GRND = DDF_air;
                FN_GRND  = FN_air;
                
            end
            
        end
        
        function [ttop_min, ttop_max] = estimate_TTOP(obj, DDT_gnd, DDF_gnd)
            
            rk = (0.56 / 2.24)^0.01;
            
            ttop_min = -999;
            ttop_max = -999;
            
            if rk*DDT_gnd - DDF_gnd <=0
                ttop_min = round(1/(365)*(-rk*DDT_gnd - DDF_gnd),2);
            end
            
            rk = (0.56 / 2.24)^0.99;
            if rk*DDT_gnd - DDF_gnd <=0
                ttop_max   = round(1/(365)*(-rk*DDT_gnd - DDF_gnd),2);
            end
            
        end
        
        function new_cmap = rescale_cmap(obj,mid_point,data_min, data_max)
            
            cmp = [[ 0.015686,  0.054902,  0.847059];...
                [ 0.12549 ,  0.313725,  1.      ];...
                [ 0.254902,  0.588235,  1.      ];...
                [ 0.427451,  0.756863,  1.      ];...
                [ 0.52549 ,  0.85098 ,  1.      ];...
                [ 0.611765,  0.933333,  1.      ];...
                [ 0.686275,  0.960784,  1.      ];...
                [ 0.807843,  1.      ,  1.      ];...
                [ 1.      ,  0.996078,  0.278431];...
                [ 1.      ,  0.921569,  0.      ];...
                [ 1.      ,  0.768627,  0.      ];...
                [ 1.      ,  0.564706,  0.      ];...
                [ 1.      ,  0.282353,  0.      ];...
                [ 1.      ,  0.      ,  0.      ];...
                [ 0.835294,  0.      ,  0.      ];...
                [ 0.619608,  0.      ,  0.      ]];
            
            pos_colors = cmp(9:16,:);
            neg_colors = cmp(1:8, :);
            
            if data_max<mid_point || data_min > mid_point
                mid_point = 0.5*(data_max + data_min);
            end
            
            n_colors    = 256;
            pos_portion = (data_max - mid_point) / (data_max - data_min);
            
            n_pos   = round(pos_portion * n_colors);
            n_neg   = n_colors - n_pos;
            pos_colors_new = zeros(n_pos, 3);
            neg_colors_new = zeros(n_neg, 3);
            
            x0 = 1:8; x0 = x0';
            x1 = 1:(7/(n_pos-1)):8;
            x1 = x1';
            
            for i =1:3
                pos_colors_new(:,i) = interp1(x0, pos_colors(:,i), x1);
            end
            
            x1 = 1:(7/(n_neg-1)):8; x1 = x1';
            
            for i =1:3
                neg_colors_new(:,i) = interp1(x0, neg_colors(:,i), x1);
            end
            
            new_cmap = [neg_colors_new;pos_colors_new];
            
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [snew,fnew,icall]=cceua(obj, s,sf,bl,bu,icall,~,FUN,varargin)
            %  This is the subroutine for generating a new point in a simplex
            %
            %   s(.,.) = the sorted simplex in order of increasing function values
            %   s(.) = function values in increasing order
            %
            % LIST OF LOCAL VARIABLES
            %   sb(.) = the best point of the simplex
            %   sw(.) = the worst point of the simplex
            %   w2(.) = the second worst point of the simplex
            %   fw = function value of the worst point
            %   ce(.) = the centroid of the simplex excluding wo
            %   snew(.) = new point generated from the simplex
            %   iviol = flag indicating if constraints are violated
            %         = 1 , yes
            %         = 0 , no
            
            [nps,nopt]=size(s);
            n = nps;
            m = nopt;
            alpha = 1.0;
            beta = 0.5;
            
            % Assign the best and worst points:
            sb=s(1,:);
            fb=sf(1);
            sw=s(n,:);
            fw=sf(n);
            
            % Compute the centroid of the simplex excluding the worst point:
            ce=mean(s(1:n-1,:));
            
            % Attempt a reflection point
            snew = ce + alpha*(ce-sw);
            
            % Check if is outside the bounds:
            ibound=0;
            s1=snew-bl;
            idx=find(s1<0, 1);
            if ~isempty(idx)
                ibound=1;
            end
            s1=bu-snew;
            idx=find(s1<0, 1);
            if ~isempty(idx)
                ibound=2;
            end
            
            if ibound >=1
                snew = bl + rand(1,nopt).*(bu-bl);
            end
            fnew = feval(FUN,snew,varargin{:});
            icall = icall + 1;
            
            % Reflection failed; now attempt a contraction point:
            if fnew > fw
                snew = sw + beta*(ce-sw);
                fnew = feval(FUN,snew,varargin{:});
                icall = icall + 1;
                
                % Both reflection and contraction have failed, attempt a random point;
                if fnew > fw
                    snew = bl + rand(1,nopt).*(bu-bl);
                    fnew = feval(FUN,snew,varargin{:});
                    icall = icall + 1;
                end
            end
            
            % END OF CCE
            return;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [bestx,bestf] = sceua(obj, FUN,x0,bl,bu,maxn,kstop,pcento,ngs,iseed,iniflg,varargin)
            
            % This is the subroutine implementing the SCE algorithm,
            % written by Q.Duan, 9/2004
            %
            % Modified by Francois Brissette to allow for additional input arguments to
            % the function and to allow the user to specify any function name through
            % argument FUN (2008)
            %
            % Definition:
            %  x0 = the initial parameter array at the start;
            %     = the optimized parameter array at the end;
            %  f0 = the objective function value corresponding to the initial parameters
            %     = the objective function value corresponding to the optimized parameters
            %  bl = the lower bound of the parameters
            %  bu = the upper bound of the parameters
            %  iseed = the random seed number (for repetetive testing purpose)
            %  iniflg = flag for initial parameter array (=1, included it in initial
            %           population; otherwise, not included)
            %  ngs = number of complexes (sub-populations)
            %  npg = number of members in a complex
            %  nps = number of members in a simplex
            %  nspl = number of evolution steps for each complex before shuffling
            %  mings = minimum number of complexes required during the optimization process
            %  maxn = maximum number of function evaluations allowed during optimization
            %  kstop = maximum number of evolution loops before convergency
            %  percento = the percentage change allowed in kstop loops before convergency
            
            % LIST OF LOCAL VARIABLES
            %    x(.,.) = coordinates of points in the population
            %    xf(.) = function values of x(.,.)
            %    xx(.) = coordinates of a single point in x
            %    cx(.,.) = coordinates of points in a complex
            %    cf(.) = function values of cx(.,.)
            %    s(.,.) = coordinates of points in the current simplex
            %    sf(.) = function values of s(.,.)
            %    bestx(.) = best point at current shuffling loop
            %    bestf = function value of bestx(.)
            %    worstx(.) = worst point at current shuffling loop
            %    worstf = function value of worstx(.)
            %    xnstd(.) = standard deviation of parameters in the population
            %    gnrng = normalized geometri%mean of parameter ranges
            %    lcs(.) = indices locating position of s(.,.) in x(.,.)
            %    bound(.) = bound on ith variable being optimized
            %    ngs1 = number of complexes in current population
            %    ngs2 = number of complexes in last population
            %    iseed1 = current random seed
            %    criter(.) = vector containing the best criterion values of the last
            %                10 shuffling loops
            
            global BESTX BESTF ICALL PX PF
            
            peps=0.001;
            
            % Initialize SCE parameters:
            nopt=length(x0);
            npg=2*nopt+1;
            nps=nopt+1;
            nspl=npg;
            mings=ngs;
            npt=npg*ngs;
            
            bound = bu-bl;
            
            % Create an initial population to fill array x(npt,nopt):
            rand('seed',iseed);
            x=zeros(npt,nopt);
            for i=1:npt
                x(i,:)=bl+rand(1,nopt).*bound;
            end
            
            if iniflg==1; x(1,:)=x0; end
            
            nloop=0;
            icall=0;
            for i=1:npt
                %  xf(i) = HMETS_duan(x(i,:),varargin);
                xf(i) = feval(FUN,x(i,:),varargin{:});
                icall = icall + 1;
            end
            f0=xf(1);
            
            % Sort the population in order of increasing function values;
            [xf,idx]=sort(xf);
            x=x(idx,:);
            
            % Record the best and worst points;
            bestx=x(1,:); bestf=xf(1);
            worstx=x(npt,:); worstf=xf(npt);
            
            BESTF=bestf;
            BESTX=bestx;
            ICALL=icall;
            
            % Compute the standard deviation for each parameter
            xnstd=std(x);
            
            % Computes the normalized geometric range of the parameters
            gnrng=exp(mean(log((max(x)-min(x))./bound)));
            
            disp('The Initial Loop: 0');
            disp(['BESTF  : ' num2str(bestf)]);
            disp(['BESTX  : ' num2str(bestx)]);
            disp(['WORSTF : ' num2str(worstf)]);
            disp(['WORSTX : ' num2str(worstx)]);
            disp(' ');
            
            % Check for convergency;
            if icall >= maxn
                disp('*** OPTIMIZATION SEARCH TERMINATED BECAUSE THE LIMIT');
                disp('ON THE MAXIMUM NUMBER OF TRIALS ');
                disp(maxn);
                disp('HAS BEEN EXCEEDED.  SEARCH WAS STOPPED AT TRIAL NUMBER:');
                disp(icall);
                disp('OF THE INITIAL LOOP!');
            end
            
            if gnrng < peps
                disp('THE POPULATION HAS CONVERGED TO A PRESPECIFIED SMALL PARAMETER SPACE');
            end
            
            % Begin evolution loops:
            nloop = 0;
            criter=[];
            criter_change=1e+5;
            
            while icall<maxn && gnrng>peps && criter_change>pcento
                nloop=nloop+1;
                
                % Loop on complexes (sub-populations);
                for igs = 1: ngs
                    
                    % Partition the population into complexes (sub-populations);
                    k1=1:npg;
                    k2=(k1-1)*ngs+igs;
                    cx(k1,:) = x(k2,:);
                    cf(k1) = xf(k2);
                    
                    % Evolve sub-population igs for nspl steps:
                    for loop=1:nspl
                        
                        % Select simplex by sampling the complex according to a linear
                        % probability distribution
                        lcs(1) = 1;
                        for k3=2:nps
                            for iter=1:1000
                                lpos = 1 + floor(npg+0.5-sqrt((npg+0.5)^2 - npg*(npg+1)*rand));
                                idx=find(lcs(1:k3-1)==lpos, 1);
                                if isempty(idx)
                                    break
                                end
                            end
                            lcs(k3) = lpos;
                        end
                        lcs=sort(lcs);
                        
                        % Construct the simplex:
                        s = zeros(nps,nopt);
                        s=cx(lcs,:); sf = cf(lcs);
                        
                        [snew,fnew,icall]= obj.cceua(s,sf,bl,bu,icall,maxn,FUN,varargin{:});
                        
                        % Replace the worst point in Simplex with the new point:
                        s(nps,:) = snew; sf(nps) = fnew;
                        
                        % Replace the simplex into the complex;
                        cx(lcs,:) = s;
                        cf(lcs) = sf;
                        
                        % Sort the complex;
                        [cf,idx] = sort(cf); cx=cx(idx,:);
                        
                        % End of Inner Loop for Competitive Evolution of Simplexes
                    end
                    
                    % Replace the complex back into the population;
                    x(k2,:) = cx(k1,:);
                    xf(k2) = cf(k1);
                    
                    % End of Loop on Complex Evolution;
                end
                
                % Shuffled the complexes;
                [xf,idx] = sort(xf); x=x(idx,:);
                PX=x; PF=xf;
                
                % Record the best and worst points;
                bestx=x(1,:); bestf=xf(1);
                worstx=x(npt,:); worstf=xf(npt);
                BESTX=[BESTX;bestx]; BESTF=[BESTF;bestf];ICALL=[ICALL;icall];
                
                % Compute the standard deviation for each parameter
                xnstd=std(x);
                
                % Computes the normalized geometric range of the parameters
                gnrng=exp(mean(log((max(x)-min(x))./bound)));
                
                disp(['Evolution Loop: ' num2str(nloop) '  - Trial - ' num2str(icall)]);
                disp(['BESTF  : ' num2str(bestf)]);
                disp(['BESTX  : ' num2str(bestx)]);
                disp(['WORSTF : ' num2str(worstf)]);
                disp(['WORSTX : ' num2str(worstx)]);
                disp(' ');
                
                % Check for convergency;
                if icall >= maxn
                    disp('*** OPTIMIZATION SEARCH TERMINATED BECAUSE THE LIMIT');
                    disp(['ON THE MAXIMUM NUMBER OF TRIALS ' num2str(maxn) ' HAS BEEN EXCEEDED!']);
                end
                
                if gnrng < peps
                    disp('THE POPULATION HAS CONVERGED TO A PRESPECIFIED SMALL PARAMETER SPACE');
                end
                
                criter=[criter;bestf];
                if (nloop >= kstop)
                    criter_change=abs(criter(nloop)-criter(nloop-kstop+1))*100;
                    criter_change=criter_change/mean(abs(criter(nloop-kstop+1:nloop)));
                    if criter_change < pcento
                        disp(['THE BEST POINT HAS IMPROVED IN LAST ' num2str(kstop) ' LOOPS BY ', ...
                            'LESS THAN THE THRESHOLD ' num2str(pcento) '%']);
                        disp('CONVERGENCY HAS ACHIEVED BASED ON OBJECTIVE FUNCTION CRITERIA!!!')
                    end
                end
                
                % End of the Outer Loops
            end
            
            disp(['SEARCH WAS STOPPED AT TRIAL NUMBER: ' num2str(icall)]);
            disp(['NORMALIZED GEOMETRIC RANGE = ' num2str(gnrng)]);
            disp(['THE BEST POINT HAS IMPROVED IN LAST ' num2str(kstop) ' LOOPS BY ', ...
                num2str(criter_change) '%']);
            
            % END of Subroutine sceua
            return;
        end
        
        function  rel_path = relativepath(obj, tgt_path, act_path )
            %RELATIVEPATH  returns the relative path from an actual path to the target path.
            %   Both arguments must be strings with absolute paths.
            %   The actual path is optional, if omitted the current dir is used instead.
            %   In case the volume drive letters don't match, an absolute path will be returned.
            %   If a relative path is returned, it always starts with '.\' or '..\'
            %
            %   Syntax:
            %      rel_path = RELATIVEPATH( target_path, actual_path )
            %
            %   Parameters:
            %      target_path        - Path which is targetted
            %      actual_path        - Start for relative path (optional, default = current dir)
            %
            %   Examples:
            %      relativepath( 'C:\local\data\matlab' , 'C:\local' ) = '.\data\matlab\'
            %      relativepath( 'A:\MyProject\'        , 'C:\local' ) = 'a:\myproject\'
            %
            %      relativepath( 'C:\local\data\matlab' , cd         ) is the same as
            %      relativepath( 'C:\local\data\matlab'              )
            %
            %   See also:  ABSOLUTEPATH PATH
            %   Jochen Lenz
            % 2nd parameter is optional:
            if  nargin < 2
                act_path = cd;
            end
            % Predefine return string:
            rel_path = '';
            % Make sure strings end by a filesep character:
            if  isempty(act_path)   ||   ~isequal(act_path(end),filesep)
                act_path = [act_path filesep];
            end
            if  isempty(tgt_path)   ||   ~isequal(tgt_path(end),filesep)
                tgt_path = [tgt_path filesep];
            end
            % Convert to all lowercase:
            [act_path] = fileparts( lower(act_path) );
            [tgt_path] = fileparts( lower(tgt_path) );
            % Create a cell-array containing the directory levels:
            act_path_cell = obj.pathparts(act_path);
            tgt_path_cell = obj.pathparts(tgt_path);
            % If volumes are different, return absolute path:
            if  isempty(act_path_cell)   ||   isempty(tgt_path_cell)
                return  % rel_path = ''
            else
                if  ~isequal( act_path_cell{1} , tgt_path_cell{1} )
                    rel_path = tgt_path;
                    return
                end
            end
            % Remove level by level, as long as both are equal:
            while  ~isempty(act_path_cell)   &&   ~isempty(tgt_path_cell)
                if  isequal( act_path_cell{1}, tgt_path_cell{1} )
                    act_path_cell(1) = [];
                    tgt_path_cell(1) = [];
                else
                    break
                end
            end
            % As much levels down ('..\') as levels are remaining in "act_path":
            for  i = 1 : length(act_path_cell)
                rel_path = ['..' filesep rel_path];
            end
            % Relative directory levels to target directory:
            for  i = 1 : length(tgt_path_cell)
                rel_path = [rel_path tgt_path_cell{i} filesep];
            end
            % Start with '.' or '..' :
            if  isempty(rel_path)
                rel_path = ['.' filesep];
            elseif  ~isequal(rel_path(1),'.')
                rel_path = ['.' filesep rel_path];
            end
            return
        end
        % -------------------------------------------------
        function  path_cell = pathparts(obj, path_str)
            path_str = [filesep path_str filesep];
            path_cell = {};
            sep_pos = strfind( path_str, filesep );
            for i = 1 : length(sep_pos)-1
                path_cell{i} = path_str( sep_pos(i)+1 : sep_pos(i+1)-1 );
            end
            return
            
        end
    end
end
