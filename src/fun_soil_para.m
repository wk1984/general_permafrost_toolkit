function f = fun_soil_para(x)

% function name: functn
%     this function is used to implement core model with the
%     specific parameter set "x";

mod0 = load('mod_inital_run.mat'); % copy model object

uts = PF_Utils;

mod1 = mod0.mod1;
mod1.START_TYPE = 1;

xx = reshape(x, numel(x)/3, 3);

mod1.VWC   = xx(:,1);
mod1.UWC_b = xx(:,3);
mod1.UWC_a = xx(:,2);

for i = 1:mod1.MN
    mod1 = mod1.update(i);
end

obs = mod1.obs_data;
sim = mod1.O_TSOIL(1:mod1.MN, mod1.obs_depth_index);

obs = [obs(:)];
sim = [sim(:)];

f = uts.RMSE(sim, obs);

end